EasyMn 
======

Descripción
-----------

EasyMN es un programa que realiza cálculos numéricos con fines pedagógicos. Ha sido creado por el MSc. Jimmy Aguilar Mena de la Universidad Agraria de la Habana en el año 2012. Y sigue siendo soportado de manera independiente por su creador hasta la fecha (2015).

El reporte de errores y comentarios puede ser reportado a la dirección personal del autor kratsbinovish@gmail.com.

El ícono que identifica a la aplicación es: 
<center><img src="forms/ui/icons/EasyMN.png" alt="Plotter" style="width:100;height:50"></center>

La principal diferencia de esta versión con las anteriores está dada por la documentación que se ha modificado para que sea mucho más completa y más fácil de mantener utilizando "Doxygen". Esta transformación mejora notablemente la estética y manejo de la misma a nivel de desarrollador.

Se ha migrado la plataforma de desarrollo avanzada a sistema Cmake con el objetivo de mejorar las características multiplataforma.

A nivel de usuario no se han agregado muchas nuevas funcionalidades, solo se han corregido una serie de errores detectados en las versiones anteriores. Las subsecciones siguen siendo:

1. ![Graficador de Curvas](forms/ui/icons/chart_curve2.png "Graficador de Curvas") Graficador de Curvas

<center><img src="screenshots/SC-Plot.png" alt="Plotter" style="width:100;height:50"></center>

2. ![Raices](forms/ui/icons/chart_curve.png "Raices") Raices de ecuaciones

<center><img src="screenshots/SC-Roots.png" alt="Plotter" style="width:100;height:50"></center>

3. ![Sistemas de ecuaciones](forms/ui/icons/application_view_columns.png "Sistemas") Sistemas de Ecuaciones

<center><img src="screenshots/SC-System.png" alt="Plotter" style="width:100;height:50">

4. ![Interpolador Polinomial](forms/ui/icons/chart_line.png "Interpolador Polinomial") Interpolador Polinomial

<center><img src="screenshots/SC-Interpolation.png" alt="Plotter" style="width:100;height:50">

5. ![Ajuste de curvas](forms/ui/icons/fitter.png "Ajuste de curvas") Ajuste de curvas

<center><img src="screenshots/SC-Fit.png" alt="Plotter" style="width:100;height:50">

6. ![Integrador Numérico](forms/ui/icons/integra.png "Integrador Numérico") Integrador Numérico

<center><img src="screenshots/SC-Integrate.png" alt="Plotter" style="width:100;height:50">

7. ![Solución de ecuaciones diferenciales](forms/ui/icons/Edo.png "Solución de ecuaciones diferenciales") Solución de ecuaciones diferenciales

<center><img src="screenshots/SC-EDO.png" alt="Plotter" style="width:100;height:50">

##TODO

Estamos centrados en este momento para la proxima version en:

###Arquitectura

1. Depurar la version para Windows usando 32 y 64 bit automatizando en la medida de lo posible el proceso de compilacion.
2. Soportar una version para MacOS lo que actualmente al desarrollador le resulta imposible pues no tiene dicho sistema operativo.
3. Mejorar el sistema de instalacion utilizando CPack para todos estos posibles sistemas Asi como para la generacion de ficheros .exe o .deb.
4. Terminar la documentacion. Actualmente parte de la documentacion ya esta elaborada y el resto se encuentra en formato Tex. Solo necesitamos tiempo para llevarla a Doxygen.
5. Automatizar la compilacion de MathGl en las diferentes arquitecturas que se soporten.
6. Implementar un sistema de medicion interno de tiempo que funcione de forma global para todos los métodos e incluir esta informacion en la interface y en los reportes.

###Métodos

1. Corregir y mejorar la calidad de los graficos principales y secundarios de interface.
2. Incluir Interpolacion utilizando método de Splines.
3. Incluir peso de errores en el ajuste de curvas al menos para modelos elementales y alguna informacion estadistica.
4. Perfeccionar el algoritmo de integración utilizando metodo de Gauss, para que parta de las raices de los polinomios de Legendre en el caso de ordenes arbitrarios.
5. Incluir integracion utilizando diferentes cuadraturas y perfeccionar los graficos asociados a cada método.
6. Incluir otros métodos de Runge-Kutta modificados.
7. Hacer la seccion de EDO mas explicativa y brindar reportes mas completos.

###En versiones porteriores

* Desarrollar la Interface en Ingles asi como la documentación.
* Incluir otros formatos de exportacion e importacion de datos

---

##Nota
>**Cualquier ayuda es bien recibida y necesitada (el soporte para Mac o windows seria de ayuda)

>**Atentamente**,

<p align="center"><b>MsC. Jimmy Aguilar Mena</b></p>
---


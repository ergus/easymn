/*
 * Este fichero es la implementación de la clase raices para la solución numérica de ecuaciones utilizando varios métodos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include "muParser.h"
#include "raices.h"

using namespace std;

raices::raices(string fun, double x1, double x2):funcion(fun),maximo(100000000){
	max=((x1==(min=(x1<x2?x1:x2)))?x2:x1);
	hayraiz=hay(min,max);
	}
	
bool raices::hay(double x1, double x2){
	double x=x1, res;
	mu::Parser parser;
	parser.SetExpr(funcion.c_str());
	parser.DefineVar("x", &x);
	res=parser.Eval();
	x=x2;
	res*=parser.Eval();
	return (res<0?true:false);
	}

double raices::biseccion(double error, vector<vector<double> > &iteraciones){
	if(hayraiz==true){
		double x=min, x1=min, y1, x2=max, y2, yc, err;
		double mult;
		iteraciones.clear();
		unsigned int previsor=0;
		mu::Parser parser;
		try{
			
			parser.SetExpr(funcion.c_str());
			parser.DefineVar("x", &x);
			
			vector<double> ini(1,x2);
			iteraciones.push_back(ini);
			ini[0]=fabs((x2-x1)/2);
			iteraciones.push_back(ini);
			
			y1=parser.Eval();
			x=x2;
			y2=parser.Eval();
			do{
				x=(x1+x2)/2;
				yc=parser.Eval();
				if((mult=y1*yc)>0){
					x1=x;
					y1=yc;
					}
				else if(mult<0){
					x2=x;
					y2=yc;
					}
				else{
					err=0;
					break;
					}
				(iteraciones[0]).push_back(x);
				(iteraciones[1]).push_back(err=fabs((x2-x1)/2));
				}while((err>error) and (previsor<maximo));			
			}
		catch(mu::Parser::exception_type &e){
			  std::cout << e.GetMsg() << endl;
			  return 0;
			}		

		if(previsor>=maximo){
			iteraciones.clear();
			x=0;
			}
		return x;
		}
	}
	
double raices::regulafalsi(double error,vector<vector<double> > &iteraciones){
	if(hayraiz==true){
		double x=min, x1=min, y1, x2=max, y2, yc, err, xant;
		double mult;
		iteraciones.clear();
		unsigned int previsor=0;
				
		mu::Parser parser;
		parser.SetExpr(funcion.c_str());
		parser.DefineVar("x", &x);
		
		vector<double> ini(1,x2);
		iteraciones.push_back(ini);
		ini[0]=fabs((x2-x1)/2);
		iteraciones.push_back(ini);
		
		y1=parser.Eval();
		xant=x=x2;
		y2=parser.Eval();
		do{
			x=x1-((x2-x1)/(y2-y1))*y1;
			yc=parser.Eval();
			err=fabs(x-xant);
			if((mult=y1*yc)>0){
				x1=x;
				y1=yc;
				}
			else if(mult<0){
				x2=x;
				y2=yc;
				}
			else{
				err=0;
				break;
				}
			(iteraciones[0]).push_back(xant=x);
			(iteraciones[1]).push_back(err);
			}while((err>error) and (previsor<maximo));
		if(previsor>=maximo){
			iteraciones.clear();
			x=0;
			}
		return x;
		}
	}
	
double raices::newtonraphson(string deriv, double error,vector<vector<double> > &iteraciones, bool usemax){
	string tem="("+funcion+")/("+deriv+")";
	iteraciones.clear();
	
	unsigned int previsor=0;
	double evalua, xold=(usemax?max:min), x=xold, err;
	
	mu::Parser parser;
	parser.SetExpr(tem.c_str());
	parser.DefineVar("x", &x);
	
	vector<double> ini(1,x);
	iteraciones.push_back(ini);
	ini[0]=0;
	iteraciones.push_back(ini);	
	
	do{
		previsor++;
		evalua=parser.Eval();
		x=xold-evalua;
		err=fabs(x-xold);
		xold=x;
		(iteraciones[0]).push_back(x);
		(iteraciones[1]).push_back(err);
		}while((err>error) and (previsor<maximo));
	if(previsor>=maximo){
		iteraciones.clear();
		x=0;
		}
	return x;
	}

double raices::newtonmodificado(double deriv, double error,vector<vector<double> > &iteraciones, bool usemax){
	iteraciones.clear();
	
	unsigned int previsor=0;
	double evalua, xold=(usemax?max:min), x=xold, err;
	
	mu::Parser parser;
	parser.SetExpr(funcion.c_str());
	parser.DefineVar("x", &x);
	
	vector<double> ini(1,x);
	iteraciones.push_back(ini);
	ini[0]=0;
	iteraciones.push_back(ini);	
	
	do{
		previsor++;
		evalua=parser.Eval();
		x=xold-evalua/deriv;
		err=fabs(x-xold);
		xold=x;
		(iteraciones[0]).push_back(x);
		(iteraciones[1]).push_back(err);
		}while((err>error) and (previsor<maximo));
	if(previsor>=maximo){
		iteraciones.clear();
		x=0;
		}
	return x;
	}
	
double raices::secantes(double error,vector<vector<double> > &iteraciones, double x1, double x2){
	iteraciones.clear();
	
	unsigned int previsor=0;
	double x1l=x1, y1, x2l=x2, y2, x, y, err;
	
	mu::Parser parser;
	parser.SetExpr(funcion.c_str());
	parser.DefineVar("x", &x);
	
	vector<double> ini(1,x1l);
	iteraciones.push_back(ini);
	ini[0]=0;
	iteraciones.push_back(ini);	
	x=x1l;
	y1=parser.Eval();
	x=x2l;
	y2=parser.Eval();
	
	do{
		previsor++;		
		x=x2l-((x2l-x1l)/(y2-y1))*y2;
		y=parser.Eval();
		err=fabs(x-x2l);
		(iteraciones[0]).push_back(x);
		(iteraciones[1]).push_back(err);
		x1l=x2l;
		x2l=x;
		y1=y2;
		y2=y;
		}while((err>error) and (previsor<maximo));
	if(previsor>=maximo){
		iteraciones.clear();
		x=0;
		}
	return x;	
	}

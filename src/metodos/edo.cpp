/*
 * Este fichero es la implementación de la clase edo para la solución numérica de ecuaciones diferenciales
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#include "edo.h"
#include "vector"
#include "string"
#include "integrador.h"

edo::edo(string fun, double ox0, double oy0):
funcion(fun), x0(ox0), y0(oy0)
	{
	parser=new mu::Parser();
    parser->SetExpr(funcion.c_str());
    parser->DefineVar("x",&x);
    parser->DefineVar("y",&y);
	}
//-.Euler---------
res edo::euler(double oh, double ox, vector<vector<double> > &sal){
	double isalv=fabs(ox-x0)/50, xsalv=x0, tem, tem2, y2=y0;
	isalv=((isalv<oh)?oh:isalv);
	sal.clear();
    sal.push_back(vector<double>(0));
    sal.push_back(vector<double>(0));
	x=x0; y=y0;
	sal[0].reserve(55);
	sal[1].reserve(55);
	int con=1;
	
    while(fabs(x-ox)>=oh){
		if(x>=xsalv){
			sal[0].push_back(tem=x);
			sal[1].push_back(y);
			xsalv+=isalv;
			}
		tem2=parser->Eval();
		y+=(tem2*oh);
		x+=oh;
		y2+=((con%2)*tem2*2*oh);
		con++;
		}
		sal[0].push_back(x);
		sal[1].push_back(y);
	y2=fabs(y2-y);
	if(10*(ox-x)>oh){
		double hf=(ox-x);
		sal[0].push_back(ox);
		sal[1].push_back(y+=(parser->Eval())*hf);		
		}
    sal.push_back(vector<double>(1,y2));
    return res(y,y2);
	}
//----RK2----------------
res edo::rk2(double oh, double ox, vector<vector<double> > &sal){

        double isalv=fabs(ox-x0)/50, xsalv=x0, tem, y1=y0, y2=y0, tx,ty;
	isalv=((isalv<oh)?oh:isalv);
	sal.clear();
    sal.push_back(vector<double>(0));
    sal.push_back(vector<double>(0));
	x=x0; y=y0;
	sal[0].reserve(55);
	sal[1].reserve(55);
	int con=1;
	//agrego
	double K[2];
	
    while(fabs(x-ox)>=oh){
		if(x>=xsalv){
			sal[0].push_back(tem=x);
			sal[1].push_back(y);
			xsalv+=isalv;
			}
		K[0]=(parser->Eval())*oh;
		x+=oh;
		y+=K[0];
		K[1]=(parser->Eval())*oh;
		y1+=(K[0]+K[1])/2;
		if(con%2==0){
			tx=x;
			ty=y;
			x-=oh;
			y=y2;
			K[0]=(parser->Eval())*2*oh;
			x+=2*oh;
			y+=K[0];
			K[1]=(parser->Eval())*2*oh;
			y2+=((K[0]+K[1])/2);
			x=tx;
			y=ty;
			}
		y=y1;
		con++;
		}
		sal[0].push_back(x);
		sal[1].push_back(y);
	y2=fabs(y2-y1)/3;
	if(10*(ox-x)>oh){
		double hf=(ox-x);
		sal[0].push_back(ox);
		sal[1].push_back(y+=(parser->Eval())*hf);		
		}
    sal.push_back(vector<double>(1,y2));
    return res(y,y2);;
	}
//---RK4-----

res edo::rk4(double oh, double ox, vector<vector<double> > &sal){
        double isalv=fabs(ox-x0)/50, xsalv=x0, y1=y0, y2=y0, tx=x0,ty=y0;
	isalv=((isalv<oh)?oh:isalv);
	
	sal.clear();
    sal.push_back(vector<double>(0));
    sal.push_back(vector<double>(0));
	x=x0; y=y0;
	sal[0].reserve(55);
	sal[1].reserve(55);
	int con=1;
	//agrego
	double K[4];
	const double pref[]={0,0.5,0.5,1};
	const int pref2[]={1,2,2,1};
	
    while(fabs(x-ox)>=oh){
		if(x>=xsalv){
			sal[0].push_back(tx=x);
			sal[1].push_back(y1);
			xsalv+=isalv;
			}

		y=ty=y1;
		K[0]=(parser->Eval())*oh;
		y1+=(K[0]/6);
		for(int i=1;i<4;i++){
			x=tx+pref[i]*oh;
			y=ty+pref[i]*K[i-1];
			K[i]=(parser->Eval())*oh;
			y1+=(pref2[i]*K[i]/6);
			}
		if(con%2==0){
			x=tx;
			y=y2;
			K[0]=parser->Eval()*2*oh;
			y2+=(K[0]/6);
			for(int i=1;i<4;i++){
				x=tx+pref[i]*2*oh;
				y=ty+pref[i]*2*K[i-1];
				K[i]=(parser->Eval())*2*oh;
				y2+=(pref2[i]*K[i]/6);
				}
			}
		x=tx+=oh;
		con++;
		}
	sal[0].push_back(x);
	sal[1].push_back(y1);
	y2=fabs(y2-y1)/15;
	if(10*(ox-x)>oh){
		double hf=(ox-x);
		sal[0].push_back(ox);
		sal[1].push_back(y1+=(parser->Eval())*hf);		
		}
    sal.push_back(vector<double>(1,y2));
    return res(y1,y2);;
	}

//-Predictor-Corrector	
res edo::pc(int orden, double oh, double ox, vector<double> ox1, vector<double> oy1, vector<vector<double> > &sal){
	const int pre[]={-1,  3,
					  5,-16, 23,
					 -9, 37,-59,55},
			  cor[]={1, 1,
			        -1, 8, 5,
				     1,-5,19,9};
	double *vx=new double[orden], *vy=new double[orden], *vf=new double[orden], x1, y1, temsum, temf, pre2[]={2,12,24};
	int i, j, tip[]={0,2,5}, orig=tip[orden-2], con=0;
	
	sal.clear();
    sal.push_back(vector<double>(1,x=vx[0]=x0));
    sal.push_back(vector<double>(1,y=vy[0]=y0));
	sal[0].reserve(60);
	sal[1].reserve(60);
	
	vf[0]=parser->Eval();
	
	for(i=1;i<orden;i++){
		x=vx[i]=ox1[i-1];
		y=vy[i]=oy1[i-1];
		sal[0].push_back(x);
		sal[1].push_back(y);
		vf[i]=parser->Eval();
		}
	x1=vx[orden-1];
	y1=vy[orden-1];
	double isalv=fabs(ox-x1)/50, xsalv=x1;
	isalv=((isalv<oh)?oh:isalv);
	
    while(fabs(x1-ox)>=oh){
		temsum=0;					//Predictor
		for(i=0;i<orden;i++){
			j=(con+i)%orden;
			temsum+=(pre[orig+i]*vf[j]);
			}
		j=con%orden;
		x1+=oh;
		temsum*=(oh/(pre2[orden-2]));
		
		
		x=vx[j]=x1;
		y=vy[j]=y1+temsum;
		vf[j]=parser->Eval();
		con++;
		
		temsum=0;					//Corrector
		for(i=0;i<orden;i++){
			temsum+=(cor[orig+i]*vf[((con+i)%orden)]);
			}
		temsum*=(oh/(pre2[orden-2]));
		y1+=temsum;
		
		y=vy[j]=y1;
		vf[j]=parser->Eval();
		if(x>=xsalv){				//Guardar
			sal[0].push_back(x);
			sal[1].push_back(y);
			xsalv+=isalv;
			}
		
		}
	sal[0].push_back(x);
	sal[1].push_back(y);
	if(10*(ox-x)>oh){
		double hf=(ox-x);
		sal[0].push_back(ox);
		sal[1].push_back(y1+=(parser->Eval())*hf);		
		}
    return res(y1,pow(oh,orden));
	}//*/

/*
 * Este fichero es la implementación de la clase fitter para el ajuste de ecuaciones lineales y polinomios a datos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#include "polinomio.h"
#include "matriz.h"
#include "fitter.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include "muParser.h"

fitter::fitter(vecdouble x, vecdouble y){
	nodos.clear();
	if(x.size()==y.size()){
		nodos.push_back(x);
		nodos.push_back(y);
		}
	}
	
polinomio fitter::polinomialfit(int grado){
	unsigned int puntos=nodos[0].size(), i, j;
	vector<double> sistsol(grado+1,0);					//para el vector solución
	vector<vector<double > > sist(grado+1,sistsol);		//para la matriz del sistema
	vector<double> linea(2*grado+1,0);
	linea[0]=(nodos[0]).size();
	double tem, val;
	
	for(j=0;j<puntos;j++){
		tem=1;
		val=((nodos[0])[j]);
		(sistsol[0])+=((nodos[1])[j]);
		for(i=1;i<=(2*grado);i++){
			tem*=val;
			linea[i]+=tem;
			if(i<=grado){
				(sistsol[i])+=(tem*((nodos[1])[j]));
				}
			}
		}
	for(i=0;i<=grado;i++){
		for(j=0;j<=grado;j++){
			(sist[i])[j]=linea[i+j];
			//cout<<((sist[i])[j])<<", ";
			}
		//cout<<sistsol[i]<<endl;
		}
	matriz matr(sist,sistsol);
	polinomio pol(matr.gauss());
	return pol;
	}
	
vector<double> fitter::linealmodel(vector<string> terminos){
	double nterm=terminos.size(), x;
	double ndatos=(nodos[0]).size();
	
	unsigned int i,j,k;
	mu::Parser parser;
	parser.SetArgSep(';');
	string expresion=terminos[0];
	for(i=1;i<nterm;i++){
		expresion+=";";
		expresion+=terminos[i];
		}
	cout<<"Expresión: "<<expresion<<endl;
	try{
		parser.SetExpr(expresion.c_str());
		}
	catch(mu::Parser::exception_type &e){
			std::cout << e.GetMsg() << endl;
			vector<double> ten;
			return ten;
			}
	parser.DefineVar("x", &x);
	
	vector<double> fila(nterm,0);
	vector<vector<double> > matr(nterm,fila);
	double *eval, tem;
	int nsal;
	
	for(k=0;k<ndatos;k++){
		x=(nodos[0])[k];
		eval=parser.Eval(nsal);
		for(i=0;i<nterm;i++){
			for(j=i;j<nterm;j++){
				tem=eval[i]*eval[j];
				((matr[i])[j])=((matr[j])[i])=((matr[j])[i])+tem;
				}
			tem=eval[i]*(nodos[1])[k];
			fila[i]+=tem;
			}
		}
        if(nterm>1){
            matriz lamatr(matr,fila);
            return lamatr.gauss();
            }
        else if(nterm==1){

            vector<double> loc(1,(fila[0])/((matr[0])[0]));
            return loc;
            }

	}
	
	
	
	
	

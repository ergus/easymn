/*
 * Este fichero es la implementación de la clase interpolador para la interpolación polinomial 
 * de datos utilizando los 3 método básicos del curso de método numéricos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#include "polinomio.h"
#include "interpolador.h"
#include "matriz.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>

interpolador::interpolador(vecdouble x, vecdouble y):difdiv(0){
	nodos.clear();
	if(x.size()==y.size()){
		nodos.push_back(x);
		nodos.push_back(y);
		}
	}
	
polinomio interpolador::lagrange(){
	unsigned int i,j,size=nodos[0].size();
	polinomio tem, tem2(0,0.);
	double a;	
	for(i=0;i<size;i++){
                tem=polinomio(0,1.);
		for(j=0;j<size;j++){
			if(i!=j){							//condición de exclusión
				a=(nodos[0])[j];
				tem*=polinomio(1,1.,-a);
				tem/=((nodos[0])[i]-(nodos[0])[j]);
				}
			}
		tem*=((nodos[1])[i]);
		tem2=tem2+tem;
		}
	polinomios[0]=new polinomio(tem2);			//guardo en el arreglo
	return tem2;
	}
	
polinomio interpolador::newton(){
	int size=((nodos[0]).size()-1), i, j, k;
	polinomio tem(0,(nodos[1])[0]);
	polinomio tem2(0,1.);
	for(i=0;i<size;i++){
		vector<double > a(size-i,0);
		difdiv.push_back(a);
		tem2*=polinomio(1,1.,-((nodos[0])[i]));
		(difdiv[0])[i]=((nodos[1])[i+1]-(nodos[1])[i])/((nodos[0])[i+1]-(nodos[0])[i]);
		for(j=1;j<=i;j++){
			k=i-j;
			((difdiv[j])[k])=(((difdiv[j-1])[k+1])-((difdiv[j-1])[k]))/(((nodos[0])[i+1])-((nodos[0])[k]));
			}
		tem=tem+(tem2*((difdiv[i])[0]));
		}
	polinomios[1]=new polinomio(tem);
	return tem;
	}
	
polinomio interpolador::sistema(){
	int size=(nodos[0]).size(), i, j;
	vector<double > tem(size,1);
	vector<vector<double> > vec(size,tem);	for(i=1;i<size;i++){
		for(j=0;j<size;j++){
			((vec[j])[i])=(tem[j])*=(nodos[0])[j];
			}	
		}	
	matriz mat(vec,nodos[1]);
	tem=mat.gauss();
	polinomio sal(tem);
	polinomios[2]=new polinomio(tem);
	return sal;	
	
	}
	
polinomio interpolador::newtonadd(double x, double y){
        int size=((nodos[0]).size()), i, j;
	polinomio poli(polinomios[1]), corr(1,1.,-(nodos[0])[0]);
	
	vector<double > dif((nodos[0]).size(),0);
	
	dif[0]=(y-(nodos[1])[size-1])/(x-(nodos[0])[size-1]);
	for(i=1;i<size;i++){
		j=size-i-1;
		dif[i]=(dif[i-1]-(difdiv[i-1])[j])/(x-(nodos[0])[j]);
		corr*=polinomio(1,1.,-(nodos[0])[i]);
		}
	corr*=(dif[size-1]);
	poli=poli+corr;
	return poli;
	
	}

/*
 * Este fichero resuelve istemas de ecuaciones con los 3 metodos estudiados en clases.
 * Crea una clase matriz que recibe la matriz del sistema y un vector para utilizarlo en caso de que se usen los m'etodos iterativos.
 * Tiene una funcion para imprimir la matriz y otros datos.
 * Los factores de convergencia se calculan en el constructor si el sistema es determinado.
 * Usa los vectores de std (por vagancia mia)
 * Jimmy Aguilar Mena (10/1/2013)
 * 
 */

#include "matriz.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>

using namespace std;
//Contructor 1
matriz::matriz(vector<vector<double> > matr,vector<double > vect):matrP(matr),vecSol(vect),alpha(0), 
								  betha(0), size(0), determinante(0){
	determinante=calcdet(matr);
	dominante=isdom(matr);
	if(determinante!=0){
		size=matr.size();
		matrM=calcM(matrP,vecSol);
		vecb=calcb(matrP,vecSol);
		alpha=calcalpha();
		betha=calcbetha();		
		}
	}


//Cálculo de M
vector<vector<double> > matriz::calcM(vector<vector<double> > matr,vector<double > vect){
	unsigned int i, j;
	vector<vector<double> > matM;
	vector<double> temp;												//vector temporal para agregar al final
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			temp.push_back((i==j?0:((-matr[i][j])/(matr[i][i]))));						//agregando elementos
			}
		matM.push_back(temp);											//agrego temp	
		temp.clear();												//limpio temp
		}	
	return matM;													//devuelvo la matriz
	}
	
//Cálculo de b
vector<double> matriz::calcb(vector<vector<double> > matr,vector<double > vect){
	unsigned int i;
	vector<double> vecb(vect);
	for(i=0;i<size;i++){
		vecb[i]/=((matr[i])[i])	;
		}	
	return vecb;
	}

//Calculo de alpha	
double matriz::calcalpha(vector<vector<double> > matr,vector<double > vect){						//este recibe las matrices del sistema
	vector<vector<double> > lM;
	double lsize;
	if (matr==matrP){
		lM=matrM;
		lsize=size;
		}
	else{
		lM=calcM(matr,vect);
		lsize=matr.size();
		}
	double alpha=0;
	for(unsigned int i=0;i<lsize;i++){
		double temp=0;
		for(unsigned int j=0;j<lsize;j++){
			temp+=sqrt(((lM[i])[j])*((lM[i])[j]));
			}
		alpha=((alpha>temp)?alpha:temp);
		}
	return alpha;
	}
	
double matriz::calcalpha(vector<vector<double> > matM){//esta recibe a M
	double alpha=0;
	for(unsigned int i=0;i<size;i++){
		double temp=0;
		for(unsigned int j=0;j<size;j++){
			temp+=fabs((matM[i])[j]);
			}
		alpha=((alpha>temp)?alpha:temp);
		}
	return alpha;
	}

//Calculo de betha
double matriz::calcbetha(vector<vector<double> > matr,vector<double > vect){					//este recibe las matrices del sistema
	vector<vector<double> > lM;
	double lsize;
	if (matr==matrP){
		lM=matrM;
		lsize=size;
		}
	else{
		lM=calcM(matr,vect);
		lsize=matr.size();
		}
	double betha=0, temp, temq;
	unsigned int i, j;
	for(i=0;i<lsize;i++){
		temp=0, temq=0;
		for(j=0;j<i;j++){
			temp+=fabs((lM[i])[j]);
			}
		for(j=i+1;j<lsize;j++){
			temq+=fabs((lM[i])[j]);
			}
		temq/=(1-temp);
		betha=((betha>temq)?betha:temq);
		}
	return betha;
	}
	
double matriz::calcbetha(vector<vector<double> > matr){								//esta recibe a M
	double betha=0, temp, temq;
	unsigned int i, j;
	for(i=0;i<size;i++){
		temp=0, temq=0;
		for(j=0;j<i;j++){
			temp+=fabs((matr[i])[j]);
			}
		for(j=i+1;j<size;j++){
			temq+=fabs((matr[i])[j]);
			}
		temq/=(1-temp);
		betha=((betha>temq)?betha:temq);
		}
	return betha;
	}
	
vector<double> matriz::gauss(vector<vector<double> > matr,vector<double > vect){
	if(size==vect.size()){
		vector<vector<double> > lmatr(matr);
		vector<double> lvect(vect), temp;
		vector<int> indice(size);
		int tempint;
		double tempd;
		unsigned int i, j, k;		
		//inicializo el índice
		for(unsigned int i=0;i<size;i++){
			indice[i]=i;
			}		
		
		for(i=0;i<size;i++){
			double max=((lmatr[i])[i])*((lmatr[i])[i]);
			unsigned int mf=i, mc=i;
			for(j=i;j<size;j++){
				for(k=i;k<size;k++){
					if(double comp=((lmatr[j])[k])*((lmatr[j])[k])>max){
						mf=j;
						mc=k;
						max=comp;
						}
					}
				}
			//pivoteo las filas
			if(mf!=i){
				temp=lmatr[mf];
				lmatr[mf]=lmatr[i];
				lmatr[i]=temp;
					
				tempd=lvect[mf];
				lvect[mf]=lvect[i];
				lvect[i]=tempd;
				}
			//pivoteo las columnas
			if(mc!=i){
				for(j=0;j<size;j++){
					tempd=((lmatr[j])[mc]);
					((lmatr[j])[mc])=((lmatr[j])[i]);
					((lmatr[j])[i])=tempd;
					}
				
				tempint=indice[mc];
				indice[mc]=indice[i];
				indice[i]=tempint;
				}
			//escalado
			for(j=i+1;j<size;j++){
				if((tempd=(lmatr[j])[i])==0)
					continue;
									
				for(k=i;k<size;k++){
					((lmatr[j])[k])/=tempd;
					((lmatr[j])[k])*=(lmatr[i])[i];
					((lmatr[j])[k])-=(lmatr[i])[k];
					}
				(lvect[j])/=tempd;
				(lvect[j])*=(lmatr[i])[i];
				(lvect[j])-=(lvect[i]);
				}
			}		
			//Comienza proceso inverso
            temp=lvect;
			for(i=size;i>0;i--){			
				for(j=size;j>i;j--){
					(temp[i-1])-=(temp[j-1])*((lmatr[i-1])[j-1]);
					}
				(temp[i-1])/=((lmatr[i-1])[i-1]);
				}
			vector<double> retur(size,0);
			for(i=0;i<size;i++){
				retur[indice[i]]=temp[i];
				}
			return retur;
		}
	else{
		vector<double > a;
		return a; 
		}
	}

//Método de Jacobi	
vector<double> matriz::jacobi(double error, vector<double > vecini, vector<vector<double> > &iteraciones, vector<vector<double> > matr,vector<double > vect){
	iteraciones.clear();
	iteraciones.push_back(vecini);	
	cout<<"Jacobi"<<endl;
	double lalpha;
	unsigned int lsize, i, j;
	vector<vector<double> > lM;
	vector<double> lb;
	if (matr==matrP){
		lM=matrM;
		lsize=size;
		lalpha=alpha;
		lb=vecb;
		}
	else{
		lM=calcM(matr,vect);
		lsize=matr.size();
		lalpha=calcalpha(lM);
		lb=calcb(matr,vect);
		}	
	if(lalpha<1){														//comprueba convergencia
		double err;
		vector<double> lsol(vecini), sal(lsize,0);
		double fac=1;
		if(lalpha>=0.5)													//para calc error;
			fac=(lalpha/(1-lalpha));		
		double diff;
		do{
			err=0;
			for(i=0;i<lsize;i++){
				sal[i]=0;
				for(j=0;j<lsize;j++){
					sal[i]+=(((lM[i])[j])*(lsol[j]));
					}
				(sal[i])+=lb[i];
				diff=fabs((lsol[i])-(sal[i]));
				err=(err>diff?err:diff);
				}
			err*=fac;
			iteraciones.push_back(lsol=sal);
			(*(iteraciones.end()-1)).push_back(err);
			}while(error<err);
		return sal;
		}
	else{
		vector<double > a;
		return a; 
		}
	}
	
vector<double> matriz::seidel(double error, vector<double > vecini, vector<vector<double> > &iteraciones, vector<vector<double> > matr,vector<double > vect){
	iteraciones.clear();
	cout<<"seidel"<<endl;
	double lbetha, tempd;
	unsigned int lsize,	i,	j;
	vector<vector<double> > lM;
	vector<double> lb;
	if (matr==matrP){
		lM=matrM;
		lsize=size;
		lbetha=betha;
		lb=vecb;
		}
	else{
		lM=calcM(matr,vect);
		lsize=matr.size();
		lbetha=calcbetha(lM);
		lb=calcb(matr,vect);
		}
	if(lbetha<1){
		double err;
		vector<double> lsol(vecini);
		double fac=1;
		if(lbetha>=0.5)
			fac=(lbetha/(1-lbetha));
		double diff;
		do{
			err=0;
			for(i=0;i<size;i++){
				tempd=0;
				for(j=0;j<size;j++){
					tempd+=(((lM[i])[j])*(lsol[j]));
					}
				tempd+=lb[i];
				diff=fabs((lsol[i])-tempd);
				lsol[i]=tempd;
				err=(err>diff?err:diff);
				}
			err*=fac;
			iteraciones.push_back(lsol);
			(*(iteraciones.end()-1)).push_back(err);			
			}while(error<err);
		return lsol;
		}
	else{
		vector<double > a;
		return a;
		} 
	}
	
//Mostrar el sistema
void matriz::printsis(vector<vector<double> > matr,vector<double > vect){
	for(unsigned int i=0;i<size;i++){
		for(unsigned int j=0;j<size;j++){
			cout<<((matr[i])[j])<<"\t";
			}
		cout<<"= "<<vect[i]<<endl;
		}
	}
	
void matriz::printsis(){
	cout<<"Imprimiendo"<<" size= "<<size<<endl;
	for(unsigned int i=0;i<size;i++){
		for(unsigned int j=0;j<size;j++){
			cout<<((matrP[i])[j])<<"\t";
			}
		cout<<"= "<<vecSol[i]<<endl;
		}
	}
	
//Comprobar si el sistema es cuadrado
bool matriz::iscuadrada(vector<vector<double> > matr){
	unsigned int lsize=matr.size(), nocua=0, temp;	
	for(unsigned int i=0;i<size;i++){
		temp=(matr[i]).size();
		nocua+=((temp==lsize)?0:1);
		}
	return (nocua==0?true:false);
	}
	
//Comprobar si es dominante
bool matriz::isdom(vector<vector<double> > matr){
	unsigned int lsize=matr.size(), nodom=0, temp;	
	for(unsigned int i=0;i<lsize;i++){
		temp=0;
		for(unsigned int j=0;j<lsize;j++){
			temp+=((i!=j)?((matr[i])[j]):0);
			}
		nodom+=(temp<((matr[i])[i])?0:1);
		}
	return (nodom==0?true:false);
	}

//Calcular el determinante
double matriz::calcdet(vector<vector<double> > matr){
	unsigned int i, j, lsize=matr.size(), temp;
        double pos, neg, det=0;
        if(lsize>2){
            for(i=0;i<lsize;i++){
                    pos=1; neg=1;
                    for(j=0;j<lsize;j++){
                            temp=((i+j)%lsize);
                            pos*=(matr[j])[temp];
                            neg*=(matr[lsize-1-j])[temp];
                            }
                    det=det+pos-neg;
                    }
        }
        else if(lsize==2){
            det=((matr[0])[0])*((matr[1])[1])-((matr[0])[1])*((matr[1])[0]);
        }

	return det;
	}

void matriz::printmat(vector<vector<double> > matr){
	vector< std::vector<double> >::iterator iter_ii;
	vector< double >::iterator iter_jj;
	for(iter_ii=matr.begin(); iter_ii!=matr.end();iter_ii++){
			for(iter_jj=(*iter_ii).begin(); iter_jj!=(*iter_ii).end();iter_jj++){
				cout<<*iter_jj<<",\t";
				}
			
			cout<<endl;
			}
	cout<<"-----"<<endl;
	}

void matriz::printmat(vector<double> matr){
	vector< double >::iterator iter_ii;
	for(iter_ii=matr.begin(); iter_ii!=matr.end();iter_ii++){
		cout<<*iter_ii<<endl;
		}
	cout<<endl;
	}	

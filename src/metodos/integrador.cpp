/*
 * Este fichero es la implementación de la clase Integrador para la integración numérica de funciones.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 * Ha sido modificada para la inclusión y perfeccionamiento de otros métodos de rombert.
 */

#include "integrador.h"
#include "muParser.h"
#include "vector"

integrador::integrador(string fun, double omin, double omax, double oh):
    funcion(fun), min(omin), max(omax), h(oh)
{
    parser=new mu::Parser();
    parser->SetExpr(funcion.c_str());
    parser->DefineVar("x",&x);
    }

integrador::~integrador(){
    delete parser;
    }
//---------trapecios------------------------
res integrador::trapecios(double lh){
    x=min;
    double xfin, sum=(parser->Eval())/2, tem1, dif, sum2=sum;
    int npasos=(max-min)/lh, con=0;
    xfin=min+npasos*lh;
    x+=lh;
    while(x<xfin){
		con++;
        sum+=tem1=parser->Eval();
        sum2+=((con%2==0)?tem1:0.);
        x+=lh;
        }
    sum+=((tem1=parser->Eval())/2); sum2+=tem1;    
    sum*=lh; sum2*=2*lh;
    
    x=max;
    tem1+=parser->Eval();
    tem1*=((max-xfin)/2);
    sum+=tem1; sum2+=tem1;
    
    sum2=fabs(sum2-sum)/3;
    return res(sum, sum2);
    }
    

//--------simpson---------------------
res integrador::simpson(double lh){
	x=min;
	double xfin, sum=parser->Eval(), I=0, P=0,tem1, tem2, tem3, dif, sum2=sum, I2=0, P2=0;
	int npasos=(max-min)/(2*lh), con=0;
	xfin=min+npasos*2*lh;
	x+=lh;
	 while(x<xfin){
		 con++;
		 tem1=parser->Eval();
		 I+=tem1;
		 x+=lh;
		 P+=tem2=parser->Eval();
		 x+=lh;
		 if(con%2==0){
			 I2+=tem1;
			 P2+=tem3=tem2;
			 }
		 }
	sum=(sum+2*P+4*I-tem2)*lh/3;
	sum2=(sum2+2*P2+4*I2-tem3)*2*lh/3;
	if((dif=max-xfin)>lh){
		x=xfin+lh;
		tem1=tem1*lh+(parser->Eval())*dif;
		dif-=lh;
		}
	else
		tem1*=dif;
	x=max;
	tem1=(tem1+(parser->Eval())*dif)/2;
	sum2=fabs(sum2-sum)/15;
	sum+=tem1;
        return res(sum,sum2);
	}
	
//----Gauss--------------------------
double integrador::gauss(int m, double omin, double omax){
	double t[]={0.57735027,
				0.77459667,
				0.86113631,0.33998104,
				0.90617985,0.53846931,
				0.93246951,0.66120939,0.23861919,
				0.94910791,0.74153119,0.40584515,
				0.96028986,0.79666648,0.52553242,0.18343464},
		   A[]={1,
				0.55555556,0.88888889,
				0.34785489,0.65214516,
				0.23692688,0.47862868,0.56888889,
				0.17132450,0.36076158,0.46791394,
				0.12948496,0.27970540,0.38183006,0.41795918,
				0.10122854,0.22238104,0.31370664,0.36268378},
			sum=0,
			tem1=(omax+omin)/2,
			tem2=(omax-omin)/2,
			tem3;
			
	int n1[]={1,1,2,2,3,3,4},
		n2[]={1,2,2,3,3,4,4},
		orig1=0, orig2=0, i, nter=n1[m-2];
		
	for(i=0;i<m-2;i++){
		orig1+=(n1[i]);
		orig2+=(n2[i]);
		} 
				
	for(i=0;i<nter;i++){
		x=tem1+tem2*t[orig1+i];
		tem3=parser->Eval();
		x=tem1-tem2*t[orig1+i];
		tem3+=parser->Eval();
		tem3*=A[orig2+i];
		sum+=tem3;
		}
	if(m%2!=0){
		x=tem1;
		sum+=(A[orig2+i]*(parser->Eval()));
		}
	return tem2*sum;
	}

res integrador::gaussfull(int m){
	double I=gauss(m), cent=(max+min)/2, I1=gauss(m,min,cent), I2=gauss(m,cent, max), error=fabs(I2-I1)/(pow(4.,m)-1);
        return res(I,error);
	}

//----rombert-----------------------------------
res integrador::rombert(double err, double oh, vector<vector<double> > &its, string met){

	double error=0, lh=oh, dtem;;
	int con=0, i, pref, p2;
	
	if(met=="trapecios"){
        f=&integrador::trapecios;               //Asigno el puntero a una funci'on o la otra
		p2=4;
		}
    else if(met=="simpsons"){
        f=&integrador::simpson;
		p2=16;
		}

    its.empty();
    vector<double> tem(1,(this->*f)(oh).val);
	its.push_back(tem);
	do{
		con++;
		tem.clear();
		lh/=2;
        tem.push_back((this->*f)(lh).val);
		for(i=0;i<con;i++){
			pref=pow(p2,i+1);
			dtem=((pref*tem[i])-its[con-1][i])/(pref-1);
			tem.push_back(dtem);
			}
		its.push_back(tem);
		error=abs(dtem-its[con-1][i-1]);
		}while((error>err) and con<100000);
		
        return res(dtem,error);
	}

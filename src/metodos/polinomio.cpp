/*
 * Este fichero es la implementación de la clase polinomio. Esta es una clase auxiliar que utilizan varios de los métodos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#include "polinomio.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>

using namespace std;
polinomio::polinomio():pol(0){
	}

polinomio::polinomio(polinomio* poli){
	this->pol=poli->getpol();
	}
	
polinomio::polinomio(vector<double> a){
	this->pol=a;
	}

polinomio::polinomio(unsigned int grado, ...){
	//Puntero a la lista variable
 	va_list ap;
  	//Inicializamos el puntero a partir del
 	//primer argumento (n)
 	va_start(ap, grado);
 	
 	vector<double> tem(grado+1,0);
 	pol=tem;
 	int n=grado;
   	while (n>=0) {
 		//Leemos el siguiente argumento
 		 pol[n]= va_arg(ap, double);
 		 n--;
		}
  	//Limpiamos
 	va_end(ap);
} 


//miscelaneas
string polinomio::tostring(){
	stringstream g;
	int tem=pol.size()-1;
	for(int i=tem;i>=0;i--){
		if((i<tem) and (pol[i]>0))
			g<<"+";
		if(pol[i]!=0){
			g<<pol[i];
			if(i>0){
				g<<"*x";
				if(i>1){
					g<<"^"<<i;
					}
				}	
			}
		}
	return g.str();
	}


void polinomio::printPol(){
	cout<<tostring()<<endl;
	}

//operadores
//suma
polinomio polinomio::operator+(polinomio h){
	unsigned int size[2], min, max, i;
	size[0]=this->getsize();
	size[1]=h.getsize();
	min=(((size[0])<(size[1]))?0:1);
	max=((min==0)?1:0);
	
	vector<double> sal(size[max],0);
	for(i=0;i<size[min];i++){
		sal[i]=pol[i]+(h.getpol(i));
		}
	for(i=size[min];i<size[max];i++){
		sal[i]=((max==0)?pol[i]:(h.getpol(i)));
		}
	polinomio polsa(sal);
	polsa.revisate();
	return polsa;
	}
	
polinomio polinomio::operator-(polinomio h){
	polinomio tem=h*(-1.);
	return (*this)+tem;
		
	}
	
//multiplicación
polinomio polinomio::polinomio::operator*(polinomio h){
	unsigned int size1=pol.size(), size2=h.getsize(), i, j;
	vector<double > sal((size1+size2-1),0);
	for(i=0;i<size1;i++){
		for(j=0;j<size2;j++){
			sal[i+j]+=(pol[i])*(h.getpol(j));
			}
		}
	polinomio polsa(sal);
	if((sal.back())==0)
		polsa.revisate();
	return polsa;
	}
	
	
polinomio polinomio::operator*(double h){
	vector<double > sal=pol;
	for(unsigned int i=0;i<pol.size();i++){
		sal[i]*=h;
		}
	polinomio polsa(sal);
	if((sal.back())==0)
		polsa.revisate();
	return polsa;
	}
	
	
//división
polinomio polinomio::operator/(double h){
	vector<double > sal=pol;
	for(unsigned int i=0;i<pol.size();i++){
		sal[i]/=h;
		}
	polinomio polsa(sal);
	return polsa;
	}
	
//Incrementales
void polinomio::operator/=(double h){
	for(unsigned int i=0;i<pol.size();i++){
		pol[i]/=h;
		}
	}
	
void polinomio::operator*=(double h){
	for(unsigned int i=0;i<pol.size();i++){
		pol[i]*=h;
		}
	}
	
void polinomio::operator*=(polinomio h){
	unsigned int size1=pol.size(), size2=h.getsize(), i, j;
	vector<double > sal((size1+size2-1),0);
	for(i=0;i<size1;i++){
		for(j=0;j<size2;j++){
			sal[i+j]+=(pol[i])*(h.getpol(j));
			}
		}
	pol=sal;
	if((sal.back())==0)
		revisate();
	}
	
//evaluador
double polinomio::eval(double c){
	double sal(pol[0]), x=1;
	int con=pol.size();
	for(unsigned int i=1;i<con;i++){
		x*=c;
		sal+=pol[i]*x;
		}
	return sal;	
	}
	
//elimina ceros
void polinomio::revisate(){
	unsigned int gra=0, graini=pol.size();	
	for(unsigned int i=0;i<graini;i++){
		gra=((pol[i]!=0)?i:gra);
		}
	if((gra+1)<graini){
		pol.erase(pol.begin()+gra+1,pol.end());
		}
	}

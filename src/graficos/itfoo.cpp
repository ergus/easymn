#include "itfoo.h"
#include "myfoo.h"
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qmathgl.h"

using namespace std;
itfoo::itfoo(mglData data, mglData error): itdata2(data), iterr2(error)
{

}

int itfoo::Draw(mglGraph *gr)
{
    double ymin=itdata2.Minimal(),
           ymax=itdata2.Maximal();

    gr->SetRange('x',0,itdata2.GetNx());

    gr->SubPlot(2,1,0);
    gr->SetRange('y',ymin,ymax);

    gr->Error(itdata2,iterr2,"ko");
    gr->Axis();

    gr->SubPlot(2,1,1);
    ymin=iterr2.Minimal(),
    ymax=iterr2.Maximal();
    gr->SetRange('y',ymin,ymax);
    gr->Title("Error vs Iteración");
    gr->Plot(iterr2,"+");
    gr->Axis();
    gr->Title("Valor vs Iteración");
    
    return 0;
}

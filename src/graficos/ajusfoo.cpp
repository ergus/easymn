#include "ajusfoo.h"
#include "string"

ajusfoo::ajusfoo(mglData *odx,mglData *ody, mglData *ofx,mglData *ofy, std::string tit):dx(odx), dy(ody), fx(ofx), fy(ofy), x(0), titulo(tit)
{
}

int ajusfoo::Draw(mglGraph *gr)
{

    if((dx!=NULL) && (dy!=NULL)){

        if((dy->nx)>0){
            double xmin=(*dx).Minimal(), xmax=(*dx).Maximal(), ymin=(*dy).Minimal(), ymax=(*dy).Maximal(), xrange=(xmax-xmin)/10, yrange=(ymax-ymin)/10;

            gr->SetRanges(xmin-xrange,xmax+xrange,ymin-yrange,ymax+yrange);
            gr->Plot(*dx,*dy,"rs");
            if(fy!=NULL)
                gr->Plot(*fx,*fy);

            if(x!=0){
                gr->Line(mglPoint(x,ymin-yrange),mglPoint(x,ymax+yrange),"b");

                }

            if(ymin*ymax<0)
                gr->SetOrigin(ymin,0.);

            gr->SetTicks('x',((xmax-xmin)/4),1);
            gr->SetTicks('y',((ymax-ymin)/4),1);
            gr->SetFontSize(4);
            gr->SetRotatedText(false);
            gr->Axis();
	    gr->Title(titulo.c_str());
            }
        }
    return 0;
    }

void ajusfoo::setdata(mglData *odx,mglData *ody, mglData *ofx,mglData *ofy){
dx=odx;
dy=ody;
fx=ofx;
fy=ofy;
}

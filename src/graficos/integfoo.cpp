#include "integfoo.h"

integfoo::integfoo(mglData *ofx,mglData *ofy): fx(ofx), fy(ofy){

}
int integfoo::Draw(mglGraph *gr){
 double xmin=(*fx).Minimal(), xmax=(*fx).Maximal(), ymin=(*fy).Minimal(), ymax=(*fy).Maximal(), xrange=(xmax-xmin)/10, yrange=(ymax-ymin)/10;
 gr->SetRanges(xmin-xrange,xmax+xrange,ymin-yrange,ymax+yrange);
 gr->Title("Gráfico del area de integración");
 gr->SetOrigin(xmin,0.);
 gr->Area(*fx,*fy,"cbgGyr");
 gr->SetTicks('x',((xmax-xmin)/4),1);
 gr->SetTicks('y',((ymax-ymin)/4),1);
 gr->SetFontSize(4);
 gr->SetRotatedText(false);
 gr->Axis();


}

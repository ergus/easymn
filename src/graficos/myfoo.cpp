/*
 * Este fichero es una de las clases graficadoras.
 * Específicamente es utilizado por la sección de graficado de función (mailplot.cpp)
 * Este comentario fue agregado en la versión 3.0
 * 
 */


#include "myfoo.h"
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include <QString>
#include <string>
#include <mgl2/qmathgl.h>


using namespace std;
myfoo::myfoo(mglData *datx, mglData *daty, double *min,double *max, double *x): mydatx(datx), mydaty(daty), mymin(min), mymax(max), myx(x), x(0), y(0)
{

}

int myfoo::Draw(mglGraph *gr)
{
    double ymin=mydaty->Minimal(),
           ymax=mydaty->Maximal();


gr->SetRanges(*mymin,*mymax,ymin,ymax);
gr->Plot(*mydatx,*mydaty);
if(x!=0){
    gr->Line(mglPoint(x,ymin),mglPoint(x,ymax),"r");
    gr->Ball(mglPoint(x,y),'g');
    }

if(ymin*ymax<0)
    gr->SetOrigin(*mymin,0.);
gr->SetTuneTicks(0);
gr->SetTickLen(-0.01);

gr->SetTicks('x',((mymax-mymin)/4),1);
gr->SetTicks('y',((ymax-ymin)/4),1);
gr->SetFontSize(4);
gr->SetRotatedText(false);
gr->Axis();
gr->Grid();
gr->Title("Gráfico de la Función");


return 0;
}

void myfoo::set(double ox, double oy){
    x=ox;
    y=oy;
    }

myfoo::~myfoo(){

    }

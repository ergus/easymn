#ifndef MYFOO_H
#define MYFOO_H
#include <QString>
#include "mgl2/mgl.h"
#include "mgl2/qmathgl.h"


using namespace std;

class mglDraw;

class myfoo : public mglDraw {
private:
    mglData *mydatx, *mydaty;
    double *mymin, *mymax, *myx;
    double x, y;

public:
    myfoo(mglData *datx,mglData *daty,double*, double*, double*);
    int Draw(mglGraph *gr);
    void set(double ox, double oy);
    ~myfoo();
};

#endif // MYFOO_H

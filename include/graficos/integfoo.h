#ifndef INTEGFOO_H
#define INTEGFOO_H

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"

class integfoo: public mglDraw{
private:
    mglData *fx, *fy;
public:
    integfoo(mglData *ofx=NULL,mglData *ofy=NULL);
    int Draw(mglGraph *gr);
};

#endif // INTEGFOO_H

#ifndef AJUSFOO_H
#define AJUSFOO_H

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "string"

class ajusfoo :public mglDraw{
    mglData *fx, *fy, *dx, *dy;
    double x;
    std::string titulo;
public:
    ajusfoo(mglData *odx=NULL,mglData *ody=NULL, mglData *ofx=NULL,mglData *ofy=NULL, std::string tit="Gráfico de los nodos y el ajuste");
     int Draw(mglGraph *gr);
     void setdata(mglData *odx,mglData *ody, mglData *ofx=NULL,mglData *ofy=NULL);
     void setx(double val){x=val;}
};

#endif // AJUSFOO_H

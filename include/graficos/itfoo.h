#ifndef ITFOO_H
#define ITFOO_H

#include <QString>
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"

using namespace std;

class mglDraw;

class itfoo : public mglDraw {
private:
    mglData itdata2, iterr2;
    int itsize;

public:
    itfoo(mglData data, mglData error);
    int Draw(mglGraph *gr);
};

#endif // ITFOO_H

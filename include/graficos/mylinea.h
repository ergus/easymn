#ifndef MYLINEA_H
#define MYLINEA_H

#include <QString>
#include "mgl2/mgl.h"
#include "mgl2/qmathgl.h"

using namespace std;

class mglDraw;

class mylinea : public mglDraw {
private:
    double y;

public:
    mylinea(double);
    int Draw(mglGraph *gr);
};

#endif // MYLINEA_H

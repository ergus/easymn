/*
 * Este fichero es la cabecera de la clase fitter para el ajuste de ecuaciones lineales y polinomios a datos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#ifndef FITTER_H
#define FITTER_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include "polinomio.h"
#include "matriz.h"

using namespace std;
typedef vector<double> vecdouble;

class fitter
{
private:
	vector<vector<double > > nodos;
	
public:
	//gets
	fitter(vecdouble x, vecdouble y);					//constructor
		
	//métodos
	polinomio polinomialfit(int grado);
	
	vector<double> linealmodel(vector<string>);
};


#endif // FITTER_H

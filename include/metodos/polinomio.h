/*
 * Este fichero es la cabecera de la clase polinomio. Esta es una clase auxiliar que utilizan varios de los métodos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */

#ifndef POLINOMIO_H
#define POLINOMIO_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>

using namespace std;

class polinomio
{
private:
vector<double > pol;

public:
polinomio();
polinomio(polinomio* poli);
polinomio(vector < double > a);
polinomio(unsigned int grado,...);

//gets
vector<double > getpol(){return pol;};
double getpol(unsigned int i){return pol[i];};
string tostring();

unsigned int getgrado(){return (pol.size())-1;};
unsigned int getsize(){return pol.size();};

//miscelaneas
void printPol();

//elimina ceros a la izquierda(derecha realmente)
void revisate();

//operadores

//suma
polinomio operator+(polinomio h);
polinomio operator-(polinomio h);
//multiplicación
polinomio operator*(polinomio h);
polinomio operator*(double h);
//división
polinomio operator/(double h);

//incrementales
void operator/=(double h);
void operator*=(double h);
void operator*=(polinomio h);


//evaluador
double eval(double c);
};


#endif // POLINOMIO_H


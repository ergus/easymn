/*
 * Este fichero es la cabecera de la clase interpolador para la interpolación polinomial 
 * de datos utilizando los 3 método básicos del curso de método numéricos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#ifndef INTERPOLADOR_H
#define INTERPOLADOR_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include "polinomio.h"
#include "matriz.h"

using namespace std;
typedef vector<double> vecdouble;

class interpolador
{
private:
	vector<vector<double > > nodos;
	vector<vector<double > > difdiv;						//para las diferencias divididas;
	polinomio* polinomios[3];

	
public:
	//gets
	interpolador(vecdouble x, vecdouble y);					//constructor
	vector<vector<double > > getdifdiv(){return difdiv;};
	polinomio* getlagrange(){return polinomios[0];};
	polinomio* getnewton(){return polinomios[1];};
	polinomio* getsistema(){return polinomios[2];};
	
	//métodos
	polinomio lagrange();
	polinomio newton();
	polinomio newtonadd(double x, double y);
	polinomio sistema();
};


#endif // INTERPOLADOR_H

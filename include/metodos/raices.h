/*
 * Este fichero es la cabecera de la clase raices para la solución numérica de ecuaciones utilizando varios métodos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */

#ifndef RAICES_H
#define RAICES_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>

using namespace std;

class raices
{
private:
string funcion;
double min, max;
bool hayraiz;
int maximo;

public:
raices(string fun, double x1, double x2);

bool hay(double x1, double x2);
bool hay(){return hayraiz;};

double biseccion(double error,vector<vector<double> > &iteraciones);
double regulafalsi(double error,vector<vector<double> > &iteraciones);
double newtonraphson(string deriv, double error,vector<vector<double> > &iteraciones, bool usemax=true);
double newtonmodificado(double deriv, double error,vector<vector<double> > &iteraciones, bool usemax=true);
double secantes(double error,vector<vector<double> > &iteraciones, double x1, double x2);
double secantes(double error,vector<vector<double> > &iteraciones){return secantes(error, iteraciones, min, max);};

void setmaximo(int val){maximo=val;};

};


#endif // POLINOMIO_H


/*
 * Este fichero es la cabecera de la clase fitter para el ajuste de ecuaciones lineales y polinomios a datos.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 */


#ifndef EDO_H
#define EDO_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include "muParser.h"
#include "string"
#include "integrador.h"

using namespace std;

class edo
{
private:
	string funcion;
	double x0, y0, x, y, h;
	mu::Parser *parser;

public:
	edo(string fun, double ox0, double oy0);
	~edo(){
		delete parser;
		}
    
	res euler(double oh, double ox, vector<vector<double> > &sal);
	
	res rk2(double oh, double ox, vector<vector<double> > &sal);
	
	res rk4(double oh, double ox, vector<vector<double> > &sal);

	
	res pc(int orden, double oh, double ox, vector<double> ox1, vector<double> oy1, vector<vector<double> > &sal);


};


#endif // EDO_H

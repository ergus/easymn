/*
 * Este fichero es la cabecera de la clase Integrador para la integración numérica de funciones.
 * ha sido creado para utilizarse en EasyMn v1.0.
 * Autor: Msc. Jimmy Aguilar Mena
 * Ha sido modificada para la inclusión y perfeccionamiento de otros métodos de rombert.
 */

#ifndef INTEGRADOR_H
#define INTEGRADOR_H

#include "muParser.h"
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <math.h>

using namespace std;
struct res{double val, err;
       res(double oval,double oerr){val=oval; err=oerr;};
	};

class integrador{
	private:
		string funcion;
		mu::Parser *parser;
        double min, max, h, x;

        res (integrador::*f)(double);				//Declaro puntero a funcion

	public:
	integrador(string fun,double omin, double omax, double oh);
        ~integrador();
        
        res trapecios(double oh);
        res trapecios(){return trapecios(h);}        
                
        res simpson(double oh);
	res simpson(){return simpson(h);}                

                
        double gauss(int m, double omin, double omax);
        double gauss(int m){return gauss(m, min, max);};
        res gaussfull(int m);
                
        res rombert(double err, double oh, vector<vector<double> > &its, string met);
        res rombert(double err, vector<vector<double> > &its, string met){return rombert(err,h,its,met);};
                
        };
#endif

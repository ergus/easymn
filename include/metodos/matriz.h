/*
 * Cabecera de la clase matriz.
 * Crea una clase matriz que recibe la matriz del sistema y un vector para utilizarlo en caso de que se usen los m'etodos iterativos.
 * Tiene una funcion para imprimir la matriz y otros datos.
 * Jimmy Aguilar Mena (10/1/2013)
 */


#ifndef MATRIZ_H
#define MATRIZ_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>

using namespace std;
class matriz
{
private:
	vector<vector<double> > matrP, matrM;
	vector<double > vecSol, vecb;
	double alpha, betha;
	unsigned int size;
	double determinante;
	bool dominante;
public:
    matriz(vector<vector<double> > matr,vector<double > vect);
    
    //Determinante
    double calcdet(vector<vector<double> > matr);
    double calcdet(){return calcdet(matrP);};
    
    //Calcular M y b
    vector<vector<double> > calcM(vector<vector<double> > matr,vector<double > vect);
    vector<double> calcb(vector<vector<double> > matr,vector<double > vect);
    vector<double> calcb(){return calcb(matrP, vecSol);};
    
    //Calcular Alpha
    double calcalpha(vector<vector<double> > matr,vector<double > vect);    
    double calcalpha(vector<vector<double> > matr);//este recibe las matrices del sistema
    double calcalpha(){return calcalpha(matrP, vecSol);};
    
    //Calcular Betha
    double calcbetha(vector<vector<double> > matr,vector<double > vect);
    double calcbetha(vector<vector<double> > matr);//este recibe las matrices del sistema
    double calcbetha(){return calcbetha(matrP, vecSol);};
    
    //Métodos
    vector<double> gauss(vector<vector<double> > matr,vector<double > vect);//método de Gauss
    vector<double> gauss(){return gauss(matrP,vecSol);};
    
    vector<double> jacobi(double error, vector<double > vecini, vector<vector<double> > &iteraciones, vector<vector<double> > matr,vector<double > vect);//Método de Jacobi
    vector<double> jacobi(double error, vector<double > vecini, vector<vector<double> > &iteraciones){return jacobi(error, vecini, iteraciones, matrP,vecSol);};
    
    vector<double> seidel(double error, vector<double > vecini, vector<vector<double> > &iteraciones, vector<vector<double> > matr,vector<double > vect);//Método de Jacobi
    vector<double> seidel(double error, vector<double > vecini, vector<vector<double> > &iteraciones){return seidel(error, vecini, iteraciones, matrP,vecSol);};
    
    //Miscelaneas
    void printsis(vector<vector<double> > matr,vector<double > vect);
    void printsis();
    
    void printmat(vector<vector<double> > matr);
    void printmat(vector<double> matr);
    
    bool iscuadrada(vector<vector<double> > matr);
    bool isdom(vector<vector<double> > matr);
    
    //gets
    vector<vector<double> > getmatrP(){return matrP;};
    vector<vector<double> > getmatrM(){return matrM;};
    vector<double > getvecSol(){return vecSol;};
    vector<double > getvecb(){return vecb;};
	double getalpha(){return alpha;};
	double getbetha(){return betha;};
	unsigned int getsize(){return size;};
	double getdeterminante(){return determinante;};
	double getdom(){return dominante;};
    
};

#endif // MATRIZ_H

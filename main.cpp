//#include <QtGui/QApplication>
#include "mainplot.h"
#include "mainraiz.h"
#include "mainsistema.h"
#include "maininterpol.h"
#include "mainajuste.h"
#include "mainintegra.h"
#include "mainedo.h"
#include "mainall.h"
#include <QSplashScreen>
#include <QtTest>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    
    QDesktopWidget *des=new QDesktopWidget();
    QRect size=des->screenGeometry(0);
    QPixmap pixmap("icons/Splash.png");
    QSplashScreen splash(pixmap.scaled(size.width()/4,size.height()/3));
    splash.adjustSize();
    splash.showMessage("  EasyMN 4.0\n  Creado por:\n  MSc Jimmy Aguilar Mena\n  2012-2014");//*/
    splash.show();
    a.processEvents();
    MainALL w;
    QTest::qWait(500);
    w.showMaximized();
    splash.finish(&w);
    delete des;
    return a.exec();

}

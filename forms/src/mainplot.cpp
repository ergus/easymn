#include "mainplot.h"
#include "ui_mainplot.h"
#include <mgl2/qmathgl.h>
#include "myfoo.h"
#include "mgl2/qt.h"
#include "muParser.h"
#include "string"
#include "qpainter.h"
#include "mylinea.h"
#include "qfiledialog.h"
#include "mainraiz.h"
#include "iostream"
#include "mainall.h"
#include "vector"

MainPlot::MainPlot(QWidget *parent, MainALL* omall) : mall(omall),
    QMainWindow(parent),
    ui(new Ui::MainPlot),QMGL(NULL), parser(NULL)
	{
	ui->setupUi(this);
	connect(this->ui->botPlot,SIGNAL(clicked()),this,SLOT(replot()));
	connect(this->ui->evalx,SIGNAL(textChanged(QString)),this,SLOT(setlaby(QString)));
	connect(this->ui->butraiz,SIGNAL(clicked()),this,SLOT(iraraiz()));


	connect(ui->actionPng,SIGNAL(triggered()), this, SLOT(export1()));
	connect(ui->actionJpg,SIGNAL(triggered()), this, SLOT(export2()));
	connect(ui->actionEps,SIGNAL(triggered()), this, SLOT(export3()));

	datx=new mglData(100,1);
	daty=new mglData(100,1);

	pmin=pmax=px=0;

	QMGL = new QMathGL(this->ui->scrollArea);
	foo=new myfoo(datx, daty,&pmin,&pmax,&px);

	QMGL->setDraw(foo);// for instance of class Foo:public mglDraw
	this->ui->scrollArea->setWidget(QMGL);
	QMGL->hide();

	this->QMGL->addAction(this->ui->actionJpg);
	this->QMGL->addAction(this->ui->actionPng);
	this->QMGL->addAction(this->ui->actionEps);
	this->QMGL->setContextMenuPolicy(Qt::ActionsContextMenu);
		
	connect(this->QMGL,SIGNAL(mouseClick(mreal,mreal,mreal)),
		this, SLOT(actpos(mreal,mreal,mreal)));

	}

MainPlot::~MainPlot()
{
    delete QMGL;
    delete ui;
    delete datx;
    delete daty;
    delete parser;
}

void MainPlot::actpos(mreal a, mreal b, mreal c){
    ui->evalx->setText(QString::number(px=a));

}

void MainPlot::replot(){
    pmin=this->ui->emin->text().toDouble();
    pmax=this->ui->emax->text().toDouble();
    if(pmin>pmax){
        double tem=pmin;
        pmin=pmax;
        pmax=tem;
        }
    px=pmin;
    paso=(pmax-pmin)/99;
    if(parser!=NULL)
        delete parser;

    parser=new mu::Parser();
    std::string cadfun=this->ui->efun->text().toStdString();
    parser->SetExpr((cadfun.c_str()));
    parser->DefineVar("x", &px);

    for(int i=0;i<100;i++){
	datx->a[i]=px;
        try{
            daty->a[i]=parser->Eval();
            }
        catch(mu::Parser::exception_type &e){
            std::cout << e.GetMsg() << endl;
            return;
            }
        px+=paso;
        }

    QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
    QMGL->update();
    QMGL->updateGeometry();
    QMGL->show();

    ui->evalx->clear();
    ui->lposy->setText("y=");
    px=pmin;
    double tem1=parser->Eval();
    px=pmax;
    double tem2=parser->Eval();
    this->ui->butraiz->setEnabled((tem1*tem2<0));
    this->ui->evalx->setEnabled(true);

}

void MainPlot::setlaby(QString){
double y;
px=ui->evalx->text().toDouble();
ui->lposy->setText("y="+QString::number(y=parser->Eval()));
foo->set(px,y);
QMGL->update();
}

void MainPlot::export1(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
    if (!fileName.isEmpty()){
            QMGL->exportPNG(fileName);
            }
    }

void MainPlot::export2(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
    if (!fileName.isEmpty()){
            QMGL->exportJPG(fileName);
            }
    }

void MainPlot::export3(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
    if (!fileName.isEmpty()){
            QMGL->exportEPS(fileName);
            }
    }

void MainPlot::iraraiz(){
    MainRaiz *raizploted=new MainRaiz(ui->efun->text(),pmin,pmax);
    mall->addvent(raizploted);
    this->destroy();
    raizploted->show();

}

void MainPlot::resizeEvent(QResizeEvent * event){
    QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
    QMGL->updateGeometry();
}

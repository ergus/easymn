#include "mainedo.h"
#include "ui_mainedo.h"
#include "edo.h"

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"

#include "muParser.h"
#include "edo.h"
#include "integrador.h"
#include "QMessageBox"
#include "ajusfoo.h"
#include "QFileDialog"

MainEDO::MainEDO(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainEDO), parser(NULL)
    {
	ui->setupUi(this);

	this->ui->twinicial->setHorizontalHeaderItem(0,new QTableWidgetItem("x"));
	this->ui->twinicial->setHorizontalHeaderItem(1,new QTableWidgetItem("y"));

	this->ui->twiterac->setHorizontalHeaderItem(0,new QTableWidgetItem("x"));
	this->ui->twiterac->setHorizontalHeaderItem(1,new QTableWidgetItem("y"));

	connect(this->ui->rbeuler,SIGNAL(toggled(bool)),this,SLOT(cambiamet()));
	connect(this->ui->rbrk,SIGNAL(toggled(bool)),this,SLOT(cambiamet()));
	connect(this->ui->rbpc,SIGNAL(toggled(bool)),this,SLOT(cambiamet()));

	connect(this->ui->eorden,SIGNAL(valueChanged(int)),this,SLOT(cambiaorden(int)));

	connect(this->ui->eh,SIGNAL(textChanged(QString)),this,SLOT(cambiah(QString)));
	connect(this->ui->twinicial,SIGNAL(cellChanged(int,int)),this,SLOT(cambiacell(int,int)));
	connect(this->ui->exfin,SIGNAL(textChanged(QString)),this,SLOT(cambiah(QString)));

	connect(this->ui->epasos,SIGNAL(textChanged(QString)),this,SLOT(cambiapaso(QString)));
	connect(this->ui->butcalc,SIGNAL(clicked()),this,SLOT(calcular()));

	connect(this->ui->actionPng ,SIGNAL(triggered()),this,SLOT(export1())) ;
	connect(this->ui->actionJpg ,SIGNAL(triggered()),this,SLOT(export2())) ;
	connect(this->ui->actionEps ,SIGNAL(triggered()),this,SLOT(export3())) ;
	connect(this->ui->actionGuardar_Resumen ,SIGNAL(triggered()),this,SLOT(resumen()));

	this->ui->twinicial->setItem(0,0,new QTableWidgetItem(QString::number(0.0)));
	this->ui->twinicial->setItem(0,1,new QTableWidgetItem(QString::number(1.0)));
	this->ui->eh->setText(QString::number(0.2));

	fx=new mglData(50);
	fy=new mglData(50);
	QMGL = new QMathGL(this->ui->scrollArea);
	this->foo=new ajusfoo(fx,fy, NULL, NULL, "Gráfico de la ecuación");

	QMGL->setDraw(foo);// for instance of class Foo:public mglDraw
	this->ui->scrollArea->setWidget(QMGL);
	QMGL->hide();
	this->ui->rbrk->toggle();
	
	this->QMGL->addAction(this->ui->actionJpg);
	this->QMGL->addAction(this->ui->actionPng);
	this->QMGL->addAction(this->ui->actionEps);
	this->QMGL->setContextMenuPolicy(Qt::ActionsContextMenu);
	}

MainEDO::~MainEDO()
{
    delete ui;
    delete QMGL;
    delete fx;
    delete fy;
    delete parser;
}

void MainEDO::cambiamet(){
    QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
    if (radio) {
        this->metodo=radio->objectName();
        this->nomet=radio->text();
        if(metodo=="rbeuler"){
            this->ui->eorden->setEnabled(false);
            this->ui->twinicial->setRowCount(1);
            }
        else if(metodo=="rbrk"){
            this->ui->eorden->setEnabled(true);
            this->ui->eorden->setSingleStep(2);
            this->ui->twinicial->setRowCount(1);
            }
        else if(metodo=="rbpc"){
            this->ui->eorden->setEnabled(true);
            this->ui->eorden->setSingleStep(1);
            cambiaorden(this->ui->eorden->value());
            }
        }
}
void MainEDO::cambiaorden(int val){
    if(metodo=="rbpc"){
        this->ui->twinicial->setRowCount(val);
        }

    }


void MainEDO::cambiah(QString val){
    if((!this->ui->twinicial->item(0,0)->text().isEmpty()) && (!this->ui->exfin->text().isEmpty()) && (!this->ui->eh->text().isEmpty())){
        double lmin=this->ui->twinicial->item(0,0)->text().toDouble(), lmax=this->ui->exfin->text().toDouble(), pas=this->ui->eh->text().toDouble();
        this->ui->epasos->setText(QString::number((lmax-lmin)/pas));
        }
    }

void MainEDO::cambiapaso(QString val){
    double lmin=this->ui->twinicial->item(0,0)->text().toDouble(), lmax=this->ui->exfin->text().toDouble(), pas=val.toDouble();
    this->ui->eh->setText(QString::number((lmax-lmin)/pas));
    }

void MainEDO::calcular(){
    double lmin=this->ui->twinicial->item(0,0)->text().toDouble(), lmax=this->ui->exfin->text().toDouble();
    QString fun;
    if(!this->ui->efun->text().isEmpty() && (this->ui->twinicial->item(0,0)!=0) && (this->ui->twinicial->item(0,1)!=0)){
        double xfin=this->ui->exfin->text().toDouble();
        edo ecdif(this->ui->efun->text().toStdString(),this->ui->twinicial->item(0,0)->text().toDouble(), this->ui->twinicial->item(0,1)->text().toDouble());
        int orden=this->ui->eorden->value(), i;
        res resu(0,0);
        vector<vector<double> > sal;
        if(metodo=="rbpc"){
            vector<double> ox(orden-1), oy(orden-1);
            for(i=1;i<orden;i++){
                if((this->ui->twinicial->item(i,0)!=0) &&(this->ui->twinicial->item(i,1)!=0)){
                    ox[i-1]=this->ui->twinicial->item(i,0)->text().toDouble();
                    oy[i-1]=this->ui->twinicial->item(i,1)->text().toDouble();
                    }
                else{
                    QMessageBox msgBox;
                    msgBox.setText("Faltan condiciones iniciales, para Predictor corrector orden "+QString::number(orden)+" se necesita la misma cantidad de condiciones iniciales.");
                    msgBox.exec();
                    return;
                    }
                }
            resu=ecdif.pc(orden,this->ui->eh->text().toDouble(),xfin,ox,oy,sal);
            }
        else if(metodo=="rbeuler"){
            resu=ecdif.euler(this->ui->eh->text().toDouble(),xfin,sal);
            }
        else if(metodo=="rbrk"){
            if(orden==2){
                resu=ecdif.rk2(this->ui->eh->text().toDouble(),xfin,sal);
                }
            else if(orden==4){
                resu=ecdif.rk4(this->ui->eh->text().toDouble(),xfin,sal);
                }
            }
        this->ui->lyout->setText(QString::number(resu.val)+"+/-"+QString::number(resu.err));
        double size=sal[0].size();
        this->ui->twiterac->setRowCount(size);

        if(fx!=NULL)
            delete fx;
        fx=new mglData(size);

        if(fy!=NULL)
            delete fy;
        fy=new mglData(size);

        for(i=0;i<size;i++){
            fx->a[i]=sal[0][i];
            fy->a[i]=sal[1][i];
            this->ui->twiterac->setItem(i,0, new QTableWidgetItem(QString::number(sal[0][i])));
            this->ui->twiterac->setItem(i,1, new QTableWidgetItem(QString::number(sal[1][i])));
            }
        QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
        QMGL->update();
        QMGL->updateGeometry();
        QMGL->show();

        }
    else{
        QMessageBox msgBox;
        msgBox.setText("Introduzca una función y/o condiciones iniciales");
        msgBox.exec();
        return;
        }
    }

void MainEDO::export1(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
    if (!fileName.isEmpty()){
            this->QMGL->exportPNG(fileName);
            }
    }

void MainEDO::export2(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
    if (!fileName.isEmpty()){
            this->QMGL->exportJPG(fileName);
            }
    }

void MainEDO::export3(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
    if (!fileName.isEmpty()){
            this->QMGL->exportEPS(fileName);
            }
    }

void MainEDO::resumen(){
    QString nomfile= QFileDialog::getSaveFileName(this,"Abrir fichero con datos");
    if(!nomfile.isNull()){
        ofstream file(nomfile.toStdString().c_str(),ios::app);
        if(file.is_open()){
            if(file.good()){
                file<<"Solución numérida de ecuaciones diferenciales con el método: "<<nomet.toStdString()<<" ";
                if(metodo!="rbeuler"){
                    file<<"orden "<<this->ui->eorden->text().toStdString()<<". ";
                    }
                file<<"\nCon las condiciones iniciales: \nx0="<<this->ui->twinicial->item(0,0)->text().toStdString()<<"; y0="<<this->ui->twinicial->item(0,1)->text().toStdString()<<endl;
                if(metodo=="rbpc"){
                    for(int i=1;i<ui->twinicial->rowCount();i++){
                        file<<"x"<<i<<"="<<ui->twinicial->item(i,0)->text().toStdString()<<"\t";
                        file<<"y"<<i<<"="<<ui->twinicial->item(i,1)->text().toStdString()<<endl;
                        }
                    }
                file<<"Para la función: ";
                file<<"y'(x,y)="<<this->ui->efun->text().toStdString()<<endl;
                file<<"Resultado y("<<this->ui->exfin->text().toStdString()<<")="<<this->ui->lyout->text().toStdString()<<endl;
                file<<"Algunos resultados intermedios fueron:"<<endl;
                for(int i=1;i<ui->twiterac->rowCount();i++){
                    file<<"x"<<i<<"="<<ui->twiterac->item(i,0)->text().toStdString()<<"\t";
                    file<<"y"<<i<<"="<<ui->twiterac->item(i,1)->text().toStdString()<<endl;
                    }
                file<<"------------------------------------------------"<<endl;
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                }
            file.close();
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("No se pudo abrir el archivo");
            msgBox.exec();
            }
        }
    }

void MainEDO::resizeEvent(QResizeEvent * event){
    QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
    QMGL->updateGeometry();
    }
    

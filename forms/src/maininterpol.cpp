#include "maininterpol.h"
#include "ui_maininterpol.h"
#include "QFileDialog"
#include "fstream"
#include "QMessageBox"
#include "muParser.h"
#include "interpolador.h"
#include "polinomio.h"

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "ajusfoo.h"
#include "mgl2/qt.h"
#include "mgl2/qmathgl.h"
#include "forminteriter.h"

#include "mainajuste.h"
#include "QMdiArea"

using namespace std;
MainInterpol::MainInterpol(QTableWidget *table,QWidget *parent,QMdiArea *parent2) :
    QMainWindow(parent),myparent(parent2),
    ui(new Ui::MainInterpol),
    from("rbMan"), metodo("rbsist")
{
	ui->setupUi(this);
	ui->eFun->setDisabled(true);
	ui->emin->setDisabled(true);
	ui->emax->setDisabled(true);
	ui->lfuncion->setDisabled(true);
	ui->lintervalo->setDisabled(true);
	ui->butDifdiv->setDisabled(true);
	ui->butLlen->setDisabled(true);
	ui->tableWidget->setHorizontalHeaderItem(0,new QTableWidgetItem("x"));
	ui->tableWidget->setHorizontalHeaderItem(1,new QTableWidgetItem("y"));
	ui->eposx->setEnabled(false);

	connect(this->ui->enodos,SIGNAL(valueChanged(int)),this,SLOT(setrow(int)));

	connect(this->ui->rbFich,SIGNAL(toggled(bool)),this,SLOT(cambiafrom(bool)));
	connect(this->ui->rbMan,SIGNAL(toggled(bool)),this,SLOT(cambiafrom(bool)));
	connect(this->ui->rbFun,SIGNAL(toggled(bool)),this,SLOT(cambiafrom(bool)));

	connect(this->ui->rbNew,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
	connect(this->ui->rbsist,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
	connect(this->ui->rbLag,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
	connect(this->ui->butLlen,SIGNAL(clicked()),this,SLOT(llenar()));
	connect(this->ui->butCalc,SIGNAL(clicked()),this,SLOT(calcular()));

	connect(this->ui->actionGuardar_Resumen ,SIGNAL(triggered()),this,SLOT(resumen()));
	connect(this->ui->actionPng ,SIGNAL(triggered()),this,SLOT(export1())) ;
	connect(this->ui->actionJpg ,SIGNAL(triggered()),this,SLOT(export2())) ;
	connect(this->ui->actionEps ,SIGNAL(triggered()),this,SLOT(export3())) ;
	connect(this->ui->butDifdiv,SIGNAL(clicked()),this,SLOT(verddif()));
	connect(this->ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(tohide(QTableWidgetItem*)));
	connect(this->ui->eposx,SIGNAL(textChanged(QString)),this,SLOT(cambiaposx(QString)));
	connect(this->ui->actionGuardar_Datos ,SIGNAL(triggered()),this,SLOT(exportdatos()));

	connect(this->ui->butIrAjuste, SIGNAL(clicked()),this,SLOT(IrAAjuste()));
	connect(this->ui->actionAbrir,SIGNAL(triggered()),this,SLOT(Abrir()));
	
	//pfx=NULL;
	//pfy=NULL;
	fx=new mglData(200);
	fy=new mglData(200);

	QMGL = new QMathGL(this->ui->scrollArea);
	foo=new ajusfoo(&dx,&dy,&pfx,&pfy);

	QMGL->setDraw(foo);					// for instance of class Foo:public mglDraw
	this->ui->scrollArea->setWidget(QMGL);
	
	this->QMGL->addAction(this->ui->actionJpg);
	this->QMGL->addAction(this->ui->actionPng);
	this->QMGL->addAction(this->ui->actionEps);
	this->QMGL->setContextMenuPolicy(Qt::ActionsContextMenu);
	
	QMGL->hide();
	connect(this->QMGL,SIGNAL(mouseClick(mreal,mreal,mreal)),this,SLOT(cambiagraf(mreal,mreal,mreal)));
	this->ui->rbLag->toggle();
	
	if(table!=NULL){
		int rows=table->rowCount();
		this->ui->tableWidget->setRowCount(rows);
		this->ui->enodos->setValue(rows);
		for (int i=0;i<rows;i++){
			for(int j=0;j<2;j++){
				if(table->item(i,j)!=0){
					this->ui->tableWidget->setItem(i,j,new QTableWidgetItem(table->item(i,j)->text()));
					}
				
				}
			}
		}
	this->ui->tableWidget->addAction(this->ui->actionGuardar_Datos);
	
	}

MainInterpol::~MainInterpol(){
	delete ui;
	delete QMGL;
	delete fx;
	delete fy;
	}

void MainInterpol::setrow(int a){
	nodos=a;
	this->ui->tableWidget->setRowCount(nodos);
	}

void MainInterpol::cambiafrom(bool){
	ui->eposx->setEnabled(false);
	QMGL->hide();
	QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
	if (radio) {
		from=radio->objectName();
		if(from=="rbMan"){
			ui->enodos->setDisabled(false);
			ui->eFun->setDisabled(true);
			ui->emin->setDisabled(true);
			ui->emax->setDisabled(true);
			ui->lfuncion->setDisabled(true);
			ui->lintervalo->setDisabled(true);
			ui->butDifdiv->setDisabled(true);
			ui->butLlen->setDisabled(true);
			ui->butCalc->setDisabled(false);
			ui->tableWidget->setDisabled(false);
			}
		else if(from=="rbFun"){
			ui->enodos->setDisabled(false);
			ui->eFun->setDisabled(false);
			ui->emin->setDisabled(false);
			ui->emax->setDisabled(false);
			ui->lfuncion->setDisabled(false);
			ui->lintervalo->setDisabled(false);
			ui->butDifdiv->setDisabled(true);
			ui->butLlen->setDisabled(false);
			ui->butCalc->setDisabled(true);
			ui->tableWidget->setDisabled(true);
			}
		else if(from=="rbFich"){
			ui->enodos->setDisabled(true);
			ui->eFun->setDisabled(true);
			ui->emin->setDisabled(true);
			ui->emax->setDisabled(true);
			ui->lfuncion->setDisabled(true);
			ui->lintervalo->setDisabled(true);
			ui->butDifdiv->setDisabled(true);
			ui->butLlen->setDisabled(true);
			ui->butCalc->setDisabled(true);
			ui->tableWidget->setDisabled(true);
			if(this->ui->rbFich->isChecked()){
				QString nomfile= QFileDialog::getOpenFileName(this,"Abrir fichero con datos");
				if(!nomfile.isEmpty()){
					if(importar(nomfile)){
						ui->butCalc->setEnabled(true);
						}
					}
				else{
					this->ui->rbMan->setChecked(true);
					}
				}
			}
		}
	}

void MainInterpol::cambiamet(bool){
	ui->eposx->setEnabled(false);
	QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
	if (radio) {
		metodo=radio->objectName();
		nommet=radio->text();
		this->ui->epol->clear();
		QMGL->hide();
		}
	}

void MainInterpol::llenar(){
    ui->eposx->setEnabled(false);
    if(from=="rbFun"){
        double min=ui->emin->text().toDouble(), max=ui->emax->text().toDouble();
        if(min!=max){
            if(min>max){
                double tem=max;
                max=min;
                min=tem;
                }
            int nodos=ui->enodos->value(), cont=0;
            double paso=(max-min)/(nodos-1), x=min, y;
            mu::Parser parser;
            parser.SetExpr((ui->eFun->text().toStdString().c_str()));
            parser.DefineVar("x", &x);
            for(cont=0;cont<nodos;cont++){
             try{
                y=parser.Eval();
                }
              catch(mu::Parser::exception_type &e){
                  cout << e.GetMsg() << endl;
                  return;
                }
                ui->tableWidget->setItem(cont,0,new QTableWidgetItem(QString::number(x)));
                ui->tableWidget->setItem(cont,1,new QTableWidgetItem(QString::number(y)));
                x+=paso;
                }
            ui->butCalc->setEnabled(true);
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("Error de intervalo");
            msgBox.exec();
            return;
            }
        QMGL->hide();
        }
    QMGL->update();
    }

bool MainInterpol::importar(QString nfile){
    ui->eposx->setEnabled(false);
    ifstream file(nfile.toStdString().c_str());
    if(!file.good()){
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                return false;
                }
            else{
                int cont=0;
                ui->tableWidget->setRowCount(cont);
                vector<double> vx(0), vy(0);
                try{
                    double x, y;
                    file >> x >> y;
                    while(!file.eof()) {

                       ui->tableWidget->insertRow(cont);
                       ui->tableWidget->setItem(cont,0,new QTableWidgetItem(QString::number(x)));
                       ui->tableWidget->setItem(cont,1,new QTableWidgetItem(QString::number(y)));
                       vx.push_back(x);
                       vy.push_back(y);
                       cont++;
                       file >> x >> y;
                       }
                    ui->enodos->setValue(cont);

                    dx.Set(vx);
                    dy.Set(vy);
                    QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
                    QMGL->update();
                    QMGL->updateGeometry();
                    QMGL->show();
                   }
                catch(...){
                    QMessageBox msgBox;
                    msgBox.setText("Error de importacion");
                    msgBox.exec();
                    file.close();
                    return false;
                    }
                file.close();
                return true;
            }
}

void MainInterpol::calcular(){
	int nodos=this->ui->enodos->value();
	vecdouble x(nodos), y(nodos);
	if((this->ui->tableWidget->item(0,0)!=0) && (this->ui->tableWidget->item(0,1)!=0)){
		x[0]=this->ui->tableWidget->item(0,0)->text().toDouble();
		y[0]=this->ui->tableWidget->item(0,1)->text().toDouble();
		}
	else{
		QMessageBox msgBox;
		msgBox.setText("Error en los Datos de pantalla, revíselos (parece que no hay datos)");
		msgBox.exec();
		return;
		}

	for(int i=1;i<nodos;i++){
		try{
			if((this->ui->tableWidget->item(i,0)!=0) && (this->ui->tableWidget->item(i,1)!=0)){
				x[i]=this->ui->tableWidget->item(i,0)->text().toDouble();
				y[i]=this->ui->tableWidget->item(i,1)->text().toDouble();
				}
			else{
				QMessageBox msgBox;
				msgBox.setText("Error en los Datos de pantalla, faltan datos. Agrégelos o ajuste la tabla a la cantidad de datos reales de trabajo.");
				msgBox.exec();
				return;
				}
			}
		catch(...){
			QMessageBox msgBox;
			msgBox.setText("Error en los Datos de pantalla, revíselos");
			msgBox.exec();
			return;
			}

		}
	interpolador interpol(x,y);
	if(metodo=="rbLag"){
		polin=interpol.lagrange();
		ui->epol->setText(QString::fromStdString(polin.tostring()));
		ui->butDifdiv->setEnabled(false);
		}
	else if(metodo=="rbNew"){
		polin=interpol.newton();
		ui->epol->setText(QString::fromStdString(polin.tostring()));
		difdiv=interpol.getdifdiv();
		ui->butDifdiv->setEnabled(true);
		}
	if(metodo=="rbsist"){
		polin=interpol.sistema();
		ui->epol->setText(QString::fromStdString(polin.tostring()));
		ui->butDifdiv->setEnabled(true);
		}

	dx.Set(x);
	dy.Set(y);

	double min=dx.Minimal(), max=dx.Maximal(), h=(max-min)/199;
	for(int i=0;i<200;i++){
		fx->a[i]=min+i*h;
		fy->a[i]=this->polin.eval(min+i*h);
		}
	pfx=fx;
	pfy=fy;
	QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
	QMGL->update();
	QMGL->updateGeometry();
	QMGL->show();
	this->ui->eposx->clear();
	this->ui->eposx->setEnabled(true);
	}

void MainInterpol::graficar(){
	QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
	QMGL->update();
	QMGL->updateGeometry();
	}

void MainInterpol::resizeEvent(QResizeEvent * event){
	QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
	QMGL->updateGeometry();
	}

void MainInterpol::resumen(){
	QString nomfile= QFileDialog::getSaveFileName(this,"Abrir fichero con datos");
	if(!nomfile.isNull()){
		ofstream file(nomfile.toStdString().c_str(),ios::app);
		if(file.is_open()){
		if(file.good()){
			file<<"Interpolación polinomial con el método: "<<nommet.toStdString()<<endl;
			if(from=="rbMan" || from=="rbFich"){
				int a=ui->tableWidget->rowCount(),i;
				file<<"Para "<<this->ui->enodos->text().toStdString()<<" datos:"<<endl;
				file<<"x    \ty"<<endl;
				for(i=0;i<a;i++){
					file<<(this->ui->tableWidget->item(i,0)->text().toStdString())<<"\t"<<this->ui->tableWidget->item(i,1)->text().toStdString()<<endl;
					}
				}
			else if(from=="rbFun"){
				file<<"Para "<<this->ui->enodos->text().toStdString()<<" puntos de la función ";
				file<<this->ui->eFun->text().toStdString();
				file<<" en el intervalo ["<< this->ui->emin->text().toStdString();
				file<<";"<< this->ui->emax->text().toStdString() <<"]."<<endl;
				}
			file<<"El polinomio interpolador quedó: "<<this->ui->epol->text().toStdString()<<endl;
			if(metodo=="rbNew"){
				file<<"Las diferencias divididas son (hacia la derecha): "<<endl;
				int size=difdiv.size();
				for(int i=0;i<size;i++){
					for(int j=0;j<size-i;j++){
						file<<difdiv[j][i]<<" ";
						}
					file<<endl;
					}
				}
			file<<"------------------------------------------------"<<endl;
			}
		else{
			QMessageBox msgBox;
			msgBox.setText("No se pudo abrir el archivo");
			msgBox.exec();
			}
		file.close();
		}
		else{
			QMessageBox msgBox;
			msgBox.setText("No se pudo abrir el archivo");
			msgBox.exec();
			}
		}
	}
	
void MainInterpol::tohide(QTableWidgetItem *){
	ui->epol->clear();
	QMGL->hide();
	}


void MainInterpol::export1(){
	QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
	if (!fileName.isEmpty()){
		QMGL->exportPNG(fileName);
		}
	}

void MainInterpol::export2(){
	QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
	if (!fileName.isEmpty()){
		QMGL->exportJPG(fileName);
		}
	}

void MainInterpol::export3(){
	QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
	if (!fileName.isEmpty()){
		QMGL->exportEPS(fileName);
		}
	}

void MainInterpol::verddif(){
	FormInterIter *vddif=new FormInterIter(difdiv,string("interpola"));
	vddif->show();
	}

void MainInterpol::cambiaposx(QString te){
	this->ui->lposy->setText(QString::number(this->polin.eval(evx=te.toDouble())));
	foo->setx(evx);
	QMGL->update();
	}

void MainInterpol::cambiagraf(mreal a, mreal, mreal){
	if(this->ui->eposx->isEnabled()){
		this->ui->eposx->setText(QString::number(a));
		}
	}

void MainInterpol::IrAAjuste(){
	MainAjuste *Ajuste=new MainAjuste(this->ui->tableWidget,parentWidget(),myparent);
	Ajuste->setAttribute(Qt::WA_DeleteOnClose);
	myparent->addSubWindow(Ajuste);
	Ajuste->showMaximized();
	this->close();
	}

void MainInterpol::exportdatos(){
    int ndat=this->ui->tableWidget->rowCount();
    QString nomfile= QFileDialog::getSaveFileName(this,"Abrir fichero con datos");
    if(!nomfile.isNull()){
        ofstream file(nomfile.toStdString().c_str(),ios::app);
        if(file.is_open()){
            if(file.good()){
                for(int i=0;i<ndat;i++){
                    if((this->ui->tableWidget->item(i,0)!=0)&&(this->ui->tableWidget->item(i,1)!=0)){
                        file<<(this->ui->tableWidget->item(i,0)->text().toStdString());
                        file<<"\t"<<(this->ui->tableWidget->item(i,1)->text().toStdString())<<endl;
                        }
                    else{
                        QMessageBox msgBox;
                        msgBox.setText("Error al exportar los datos, faltan datos en pantalla");
                        msgBox.exec();
                        file.close();
                        return;
                        }
                    }
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                }
            file.close();
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("No se pudo abrir el archivo");
            msgBox.exec();
            }
        }
    }

void MainInterpol::Abrir(){
	this->ui->rbFich->setChecked(true);
	}
#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::SplashScreen);
    this->setAttribute(Qt::WA_DeleteOnClose);
}

About::~About()
{
    delete ui;
}

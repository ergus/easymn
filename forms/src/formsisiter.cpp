#include "formsisiter.h"
#include "ui_formsisiter.h"

FormSisIter::FormSisIter(matri mat, QString meto, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormSisIter)
{
    unsigned int its=mat.size(), rang=(mat[1]).size();
    ui->setupUi(this);
    this->ui->tableWidget->setColumnCount(rang);
    this->ui->tableWidget->setRowCount(its);

    for(unsigned int i=0;i<rang; i++){
        if(i==(rang-1))
            this->ui->tableWidget->setHorizontalHeaderItem(i,new QTableWidgetItem("error"));
        else
            this->ui->tableWidget->setHorizontalHeaderItem(i,new QTableWidgetItem("x"+QString::number(i)));
        for(unsigned int j=0;j<its; j++){
            this->ui->tableWidget->setItem(j,i,new QTableWidgetItem(QString::number((mat[j])[i])));
            }
    }
    this->ui->label->setText("Método de "+meto+" (Para copiar guarde el resumen en la opcion Archivo)");
}

FormSisIter::~FormSisIter()
{
    delete ui;
}

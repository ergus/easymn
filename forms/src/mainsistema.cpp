#include "mainsistema.h"
#include "ui_mainsistema.h"
#include "QMessageBox"
#include "qfiledialog.h"
#include "matriz.h"
#include "QDate"
#include "formsisiter.h"

MainSistema::MainSistema(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainSistema)
{
	ui->setupUi(this);
	connect(this->ui->rbgauss,SIGNAL(toggled(bool)),this,SLOT(cambiametodo(bool)));
	connect(this->ui->rbjacobi,SIGNAL(toggled(bool)),this,SLOT(cambiametodo(bool)));
	connect(this->ui->rbseidel,SIGNAL(toggled(bool)),this,SLOT(cambiametodo(bool)));

	connect(this->ui->rbmanual,SIGNAL(toggled(bool)),this,SLOT(cambiaman(bool)));
	connect(this->ui->rbarchivo,SIGNAL(toggled(bool)),this,SLOT(cambiaarchi(bool)));

	connect(this->ui->edim,SIGNAL(valueChanged(QString)),this,SLOT(redimen(QString)));

	connect(this->ui->butcalc,SIGNAL(clicked()),this,SLOT(calcular()));

	connect(this->ui->actionGuardar_Resumen,SIGNAL(triggered()),this,SLOT(exportar()));
	connect(this->ui->actionExportar_Matriz_a_Fichero,SIGNAL(triggered()),this,SLOT(guardamat()));
	connect(this->ui->butiter,SIGNAL(clicked()),this,SLOT(veriter()));
	
	connect(this->ui->actionAbrir_2,SIGNAL(triggered()),this,SLOT(cambiaarchi()));

	this->ui->eerror->setDisabled(true);
	this->ui->tabinicial->hide();
	this->ui->lvini->setDisabled(true);

	this->ui->tabinicial->setRowCount(1);
	this->ui->tabfinal->setRowCount(1);

	this->ui->tabinicial->setVerticalHeaderItem(0,new QTableWidgetItem(" "));
	this->ui->tabfinal->setVerticalHeaderItem(0,new QTableWidgetItem(""));
	this->ui->eerror->setText(QString::number(1e-3));
	this->ui->groupBox_3->hide();
	this->ui->rbgauss->toggle();
	this->ui->tabmatriz->addAction(this->ui->actionExportar_Matriz_a_Fichero);

	}	

MainSistema::~MainSistema()
{
    delete ui;
}

void MainSistema::cambiametodo(bool){
  QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
  if (radio) {
    metodo=radio->text();
    if(metodo=="Gauss"){
        this->ui->eerror->setDisabled(true);
        this->ui->tabinicial->hide();
        this->ui->lvini->setDisabled(true);
        this->ui->butiter->setDisabled(true);
        }
    else{
        this->ui->eerror->setEnabled(true);
        this->ui->tabinicial->show();
        this->ui->lvini->setEnabled(true);
        }
    }
}

void MainSistema::cambiaarchi(bool){
    if(this->ui->rbarchivo->isChecked()){
        QString dial= QFileDialog::getOpenFileName(this,"Abrir fichero con sistema de ecuaciones");
        if(!dial.isEmpty()){
            if(importar(dial)){
                this->ui->edim->setDisabled(true);
                this->ui->edim->setValue(rango);

                for(unsigned int i=0;i<rango;i++){
                    for(unsigned int j=0;j<rango;j++){
                        this->ui->tabmatriz->setItem(i,j,new QTableWidgetItem(QString::number((matrizs[i])[j])));
                        }
                    this->ui->tabmatriz->setItem(i,rango+1,new QTableWidgetItem(QString::number(vectors[i])));
                    }
                }
            else{
                this->ui->rbmanual->setChecked(true);
                }
            }
        else{
            this->ui->rbmanual->setChecked(true);
            }
        }
    }

void MainSistema::cambiaarchi(){
	this->ui->rbarchivo->setChecked(true);
	}

void MainSistema::cambiaman(bool){
    if(this->ui->rbmanual->isChecked()){
          this->ui->edim->setDisabled(false);
    }
}


void MainSistema::redimen(QString){
    this->ui->tabmatriz->setColumnCount(0);
    rango=this->ui->edim->value();
    this->ui->tabmatriz->setColumnCount(rango+2);
    this->ui->tabmatriz->setRowCount(rango);
    this->ui->tabmatriz->setColumnWidth(rango,20);

    this->ui->tabfinal->clear();
    this->ui->tabinicial->setColumnCount(rango);
    this->ui->tabfinal->setColumnCount(rango);
    for(int i=0;i<rango;i++){
        this->ui->tabmatriz->setHorizontalHeaderItem(i,new QTableWidgetItem("x"+QString::number(i)));
        this->ui->tabinicial->setHorizontalHeaderItem(i,new QTableWidgetItem("x"+QString::number(i)));
        this->ui->tabfinal->setHorizontalHeaderItem(i,new QTableWidgetItem("x"+QString::number(i)));
        }
    this->ui->tabmatriz->setHorizontalHeaderItem(rango,new QTableWidgetItem("="));
    this->ui->tabmatriz->setHorizontalHeaderItem(rango+1,new QTableWidgetItem("y"));
    this->ui->ldet->clear();
    this->ui->lalfa->clear();
    this->ui->lbeta->clear();

}

bool MainSistema::importar(QString nfile){
    matrizs.clear();
    vectors.clear();
    FILE *file=fopen(nfile.toStdString().c_str() ,"r");
            if(file==NULL){
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                return false;
                }
            else{
                    double dtmp;
                    vector<double> vectmp;
                    fscanf(file,"%i\t\n",&rango);
                    try{
                            for(int i=0;i<rango;i++){
                                    for(int j=0;j<rango;j++){
                                            fscanf(file,"%lf",&dtmp);
                                            vectmp.push_back(dtmp);
                                            }
                                    matrizs.push_back(vectmp);
                                    vectmp.clear();
                                    fscanf(file,"%lf",&dtmp);
                                    vectors.push_back(dtmp);
                                    }
                            }
                    catch(...){
                        QMessageBox msgBox;
                        msgBox.setText("Error de importacion");
                        msgBox.exec();
                        fclose(file);
                        return false;
                        }
                    fclose(file);
                    return true;
            }
}

void MainSistema::calcular(){
this->ui->groupBox_3->show();


captapantalla();
std::vector<double> vecinis(rango,0);

matriz mat(matrizs,vectors);
    this->ui->ldet->setText(QString::number(mat.getdeterminante()));
    this->ui->lalfa->setText(QString::number(mat.getalpha()));
    this->ui->lbeta->setText(QString::number(mat.getbetha()));
    if(mat.getdeterminante()!=0){
        if(metodo=="Gauss"){
            vecsalida=mat.gauss();
            this->ui->butiter->setEnabled(false);
            this->ui->literac->text().clear();
            }
        else{
            if(mat.getalpha()<=1){
                for(unsigned int i=0;i<rango;i++){
                    if(this->ui->tabinicial->item(0,i)!=0)
                        vecinis[i]=this->ui->tabinicial->item(0,i)->text().toDouble();
                    else{
                        QMessageBox msgBox;
                        msgBox.setText("Llene el vector inicial");
                        msgBox.exec();
                        return;
                        }
                    }
                double error=this->ui->eerror->text().toDouble();
                if(metodo=="Jacobi"){
                    vecsalida=mat.jacobi(error,vecinis,iteracioness);
                    }
                else if(metodo=="Seidel"){
                    vecsalida=mat.seidel(error,vecinis,iteracioness);
                    }
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("Factor de Convergencia >= 1, c�lculo detenido\n alfa="+QString::number(mat.getalpha()));
                msgBox.exec();
                return;
                }
            this->ui->butiter->setEnabled(true);
            this->ui->literac->setText("Iteraciones: "+QString::number(iteracioness.size()));
            }
        for(unsigned int i=0;i<rango;i++){
            this->ui->tabfinal->setItem(0,i,new QTableWidgetItem(QString::number(vecsalida[i])));
            }
        }
    else{
        QMessageBox msgBox;
        msgBox.setText("Determinante nulo, vea la tabla de datos del sistema");
        msgBox.exec();
        }

}

bool MainSistema::captapantalla(){
    matrizs.clear();
    vectors.clear();
    rango=this->ui->edim->value();
    std::vector<double> vecinis(rango,0);
    try{
        for(unsigned int i=0;i<rango;i++){
            for(unsigned int j=0;j<rango;j++){
                 if(this->ui->tabmatriz->item(i,j)!=0)
                     vecinis[j]=this->ui->tabmatriz->item(i,j)->text().toDouble();

                 else{
                     QMessageBox msgBox;
                     msgBox.setText("Faltan valores en el sistema");
                     msgBox.exec();
                    }
                }
            matrizs.push_back(vecinis);
            vectors.push_back(this->ui->tabmatriz->item(i,rango+1)->text().toDouble());
            }
        return true;
        }
    catch(...){
        return false;
        }
}

void MainSistema::exportar(){
    captapantalla();
    unsigned int i,j;
    if(this->ui->tabfinal->item(0,0)!=0){
    if(!this->ui->tabfinal->item(0,0)->text().isEmpty()){
        int siz=iteracioness.size();
        QString fileName = QFileDialog::getSaveFileName(this,"Guardar los resultados en un fichero");
        std::ofstream file;
        file.open(fileName.toStdString().c_str(),std::ios::app);
        file<<"Resultados de la soluci�n del sistema:"<<endl;

        for(i=0;i<rango;i++){
            for(j=0;j<rango;j++){
                file<<matrizs[i][j]<<"\t";
                }
             file<<"|"<<vectors[i]<<endl;
            }

        file<<"Metodo de : "<<metodo.toStdString()<<endl;
        if(metodo!="Gauss"){
            file<<"Con vector inicial: [";
             for(i=0;i<rango;i++){
                 file<<(this->ui->tabinicial->item(0,i)->text().toStdString())<<";";
                }
             file<<"]."<<endl;
             file<<"Error="<<this->ui->eerror->text().toStdString()<<"\t";
            }

        file<<"Fecha: "<<QDate::currentDate().toString("ddd d MMMM yyyy").toStdString()<<endl;
        file<<"\nResultados:"<<endl;
        for(i=0;i<rango;i++){
            file<<"x"<<i<<"="<<(this->ui->tabfinal->item(0,i)->text().toStdString())<<"\t";
           }
        file<<endl;
        if(metodo!="Gauss"){
            file<<"Iteraciones:"<<endl;

            unsigned int niter=iteracioness.size();
            file<<"Iter\t";
            for(i=0;i<rango;i++){
                file<<"x"<<i<<"\t\t";
                }
            file<<"error"<<endl;
            for(i=0;i<niter;i++){
                file<<i<<"\t";
                for(j=0;j<=rango;j++){
                    file<<iteracioness[i][j]<<"\t\t";
                    }
                file<<endl;
                }
            }


        file<<"\n---------------------------------------------------------------\n";
        file.close();
        }
    else{
        QMessageBox msgBox;
        msgBox.setText("Por favor realice alg�n c�lculo para guardar");
        msgBox.exec();
        }
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Por favor realice alg�n c�lculo para guardar");
        msgBox.exec();
        }
}
 void MainSistema::guardamat(){
     if(this->ui->tabmatriz->item(0,0)!=0){
        if(!this->ui->tabfinal->item(0,0)->text().isEmpty()){
            captapantalla();
            QString fileName = QFileDialog::getSaveFileName(this,"Guardar los resultados en un fichero");
            std::ofstream file;
            file.open(fileName.toStdString().c_str(),std::ios::app);

            for(unsigned int i=0;i<rango;i++){
                for(unsigned int j=0;j<rango;j++){
                    file<<matrizs[i][j]<<"\t";
                    }
                 file<<vectors[i]<<endl;
                }
            file.close();
           }
        else{
            QMessageBox msgBox;
            msgBox.setText("No hay datos en la pantalla");
            msgBox.exec();
            }
        }
     else{
         QMessageBox msgBox;
         msgBox.setText("No hay datos en la pantalla");
         msgBox.exec();
        }

    }

 void MainSistema::veriter(){
     FormSisIter *fst=new FormSisIter(iteracioness,metodo);
     fst->show();
 }

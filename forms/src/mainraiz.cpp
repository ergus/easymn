#include "mainraiz.h"
#include "ui_mainraiz.h"
#include "myfoo.h"
#include "mgl2/qt.h"
#include <mgl2/qmathgl.h>
#include "muParser.h"
#include "raices.h"
#include "QMessageBox"
#include "vector"
#include "QFileDialog"
#include "formplotiter.h"
#include "QDate"

MainRaiz::MainRaiz(QWidget *parent) :
    QMainWindow(parent), parser(NULL),
    ui(new Ui::MainRaiz)
{
    rmin=rmax=0;
    ui->setupUi(this);


    inicializa();
    rQMGL->hide();

}


MainRaiz::MainRaiz(QString fun, double min, double max, QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainRaiz),
    rmin(min), rmax(max)

{
    ui->setupUi(this);
    this->ui->efun->setText(fun);
    this->ui->emin->setText(QString::number(min));
    this->ui->emax->setText(QString::number(max));


    inicializa();
}

MainRaiz::~MainRaiz()
{

    delete rQMGL;
    delete ui;
    delete datx;
    delete daty;
    delete parser;

}

void MainRaiz::graficar(){
        rmin=this->ui->emin->text().toDouble();
        rmax=this->ui->emax->text().toDouble();
        if(rmin>rmax){
            double tem=rmin;
            rmin=rmax;
            rmax=tem;
            }
        x=rmin;
        paso=(rmax-rmin)/49;

        parser=new mu::Parser();
        parser->SetExpr((this->ui->efun->text().toStdString().c_str()));
        parser->DefineVar("x", &x);


        for(int i=0;i<50;i++){
            try{
                daty->a[i]=parser->Eval();
                }
            catch(mu::Parser::exception_type &e){
                std::cout << e.GetMsg() << endl;
                return;
                }
            datx->a[i]=x;
            x+=paso;
            }
        rQMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
        rQMGL->update();
        rQMGL->updateGeometry();
        this->rQMGL->show();
	}

void MainRaiz::rbcheck(bool){
        ui->lmet->clear();
        ui->lvalout->clear();
        QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
        if (radio) {
          metodo=radio->objectName();
          nombremetodo=radio->text();
          if(metodo==this->ui->rbNR->objectName()){
            ui->ederiv->setEnabled(true);
            ui->emin->hide();
            ui->lint->setText("Xo:");
            ui->lderiv->setText("f'(x):");
            }
          else if(metodo==this->ui->rbNM->objectName()){
            ui->ederiv->setEnabled(true);
            ui->emin->hide();
            ui->lint->setText("Xo:");
            ui->lderiv->setText("f'(xo):");
          }
          else{
              ui->ederiv->setEnabled(false);
              ui->emin->show();
              ui->lint->setText("Intervalo:");
              ui->lderiv->setText("f'(x):");
            }
        }

}

void MainRaiz::inicializa(){
	connect(this->ui->rbbisec,SIGNAL(toggled(bool)),this,SLOT(rbcheck(bool)));
	connect(this->ui->rbregfal,SIGNAL(toggled(bool)),this,SLOT(rbcheck(bool)));
	connect(this->ui->rbNR,SIGNAL(toggled(bool)),this,SLOT(rbcheck(bool)));
	connect(this->ui->rbNM,SIGNAL(toggled(bool)),this,SLOT(rbcheck(bool)));
	connect(this->ui->rbsec,SIGNAL(toggled(bool)),this,SLOT(rbcheck(bool)));
	connect(this->ui->butgra,SIGNAL(clicked()),this,SLOT(graficar()));

	metodo=this->ui->rbbisec->objectName();
	nombremetodo=this->ui->rbbisec->text();

	connect(this->ui->butcalc,SIGNAL(clicked()),this,SLOT(calcular()));

	connect(ui->actionPNG,SIGNAL(triggered()), this, SLOT(export1()));
	connect(ui->actionJPG,SIGNAL(triggered()), this, SLOT(export2()));
	connect(ui->actionEPS,SIGNAL(triggered()), this, SLOT(export3()));
	connect(ui->actionGuardar_Tabla,SIGNAL(triggered()),this,SLOT(guardaraiz()));

	connect(ui->butgrait,SIGNAL(clicked()),this,SLOT(grafit()));

	//inicializaci�n de pantalla

	ui->ederiv->setDisabled(true);
	double err=1e-3;
	ui->eerror->setText(QString::number(err));

	iter.clear();
	datx=new mglData(50,1);
	daty=new mglData(50,1);

	foo=new  myfoo(datx, daty, &rmin,&rmax,&x);

	rQMGL = new QMathGL(this->ui->scrollArea);
	rQMGL->setDraw(foo);// for instance of class Foo:public mglDraw
	this->ui->scrollArea->setWidget(rQMGL);
	this->rQMGL->hide();
	
	this->rQMGL->addAction(this->ui->actionJPG);
	this->rQMGL->addAction(this->ui->actionPNG);
	this->rQMGL->addAction(this->ui->actionEPS);
	this->rQMGL->setContextMenuPolicy(Qt::ActionsContextMenu);
	this->ui->tableWidget->addAction(this->ui->actionGuardar_Tabla);

	}

void MainRaiz::calcular(){
    graficar();
    if(!this->ui->efun->text().isEmpty()){
        raices raiz(this->ui->efun->text().toStdString(),rmin,rmax);
        double error=ui->eerror->text().toDouble();

        bool done=false;

        if(metodo=="rbNR"){
            if(ui->ederiv->text().isEmpty()){
                QMessageBox msgBox;
                msgBox.setText("Introduzca el valor de la derivada");
                msgBox.exec();
                }
            else{
                raiz.newtonraphson(ui->ederiv->text().toStdString(),error,iter);
                done=true;
                }

            }
        else if(metodo=="rbNM"){
            if(ui->ederiv->text().isEmpty()){
                QMessageBox msgBox;
                msgBox.setText("Introduzca el valor de la derivada");
                msgBox.exec();
                }
            else{
                raiz.newtonmodificado(ui->ederiv->text().toDouble(),error,iter);
                done=true;
                }

            }

        else if(metodo=="rbsec"){
            raiz.secantes(error,iter);
            done=true;
            }

        else{
            if(raiz.hay()==true){
               if(metodo=="rbbisec"){
                    raiz.biseccion(error,iter);
                    done=true;
                    }

                else if(metodo=="rbregfal"){
                    raiz.regulafalsi(error,iter);
                    done=true;
                    }
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("No parece haber raiz e el intervalo, por favor revice la gráfica");
                msgBox.exec();
                }
            }
        if(done){
             int row=iter[0].size();

            ui->lvalout->setText(QString::number(*(iter[0].end()-1))+" +/- "+QString::number(*(iter[1].end()-1))+" en "+QString::number(row)+" iteraciones.");
            ui->lmet->setText(nombremetodo);


            //esto es en la parte de la tabla ya
            ui->lmetodo->setText("Iteraciones "+nombremetodo);
            ui->tableWidget->setColumnCount(2);

            ui->tableWidget->setRowCount(row);
            ui->tableWidget->setHorizontalHeaderItem(0,new QTableWidgetItem("Valor"));
            ui->tableWidget->setHorizontalHeaderItem(1,new QTableWidgetItem("error"));
            for(int i=0;i<row;i++){
                ui->tableWidget->setItem(i,0,new QTableWidgetItem(QString::number(iter[0][i])));
                ui->tableWidget->setItem(i,1,new QTableWidgetItem(QString::number(iter[1][i])));
                }
            this->foo->set(*(iter[0].end()-1),0.);
            this->rQMGL->update();
            this->rQMGL->show();
            }
        }
    else{
        QMessageBox msgBox;
        msgBox.setText("Introduzca una Función");
        msgBox.exec();
        }

}

void MainRaiz::export1(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
    if (!fileName.isEmpty()){
            rQMGL->exportPNG(fileName);
            }
    }

void MainRaiz::export2(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
    if (!fileName.isEmpty()){
            rQMGL->exportJPG(fileName);
            }
    }

void MainRaiz::export3(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
    if (!fileName.isEmpty()){
            rQMGL->exportEPS(fileName);
            }
    }

void MainRaiz::grafit(){
    if(iter.size()!=0){
        FormPlotIter *itform=new FormPlotIter(iter);
        itform->show();
        }
    }

void MainRaiz::guardaraiz(){
    if(iter.size()>0){
        int siz=iter[0].size();
        QString fileName = QFileDialog::getSaveFileName(this,"Guardar los resultados en un fichero");
        std::ofstream file;
        file.open(fileName.toStdString().c_str(),std::ios::app);
        file<<"Resultados del Calculo de la raiz de f(x)="<<(this->ui->efun->text().toStdString())<<endl;
        file<<"Metodo: "<<nombremetodo.toStdString()<<" con ";
        if(metodo!="rbNR" and metodo!="rbNM"){
            file<<"Intervalo: ["<<this->ui->emin->text().toStdString()<<" ; "<<this->ui->emax->text().toStdString()<<"]"<<endl;
            }
        else{
            file<<"Xo=: "<<this->ui->emax->text().toStdString()<<endl;
            file<<"f'=: "<<this->ui->ederiv->text().toStdString()<<endl<<endl;
            }
        file<<"Fecha: "<<QDate::currentDate().toString("ddd d MMMM yyyy").toStdString()<<endl;
        file<<"Raiz: "<<ui->lvalout->text().toStdString()<<endl;
        file<<"it\traiz\terror"<<endl;
        for(unsigned int i=0;i<siz;i++){
            file<<i<<"\t"<<((iter[0])[i])<<"\t"<<((iter[1])[i])<<endl;
            }
        file<<"\n---------------------------------------------------------------\n";
        file.close();
        }
    else{
        QMessageBox msgBox;
        msgBox.setText("Por favor realice algún cálculo para guardar");
        msgBox.exec();
        }
    }

void MainRaiz::resizeEvent(QResizeEvent * event){
    rQMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
    rQMGL->updateGeometry();
}

#include "forminteriter.h"
#include "ui_forminteriter.h"
#include "iostream"

FormInterIter::FormInterIter(vector<vector<double> > mat, string para, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormInterIter)
{
    ui->setupUi(this);

    int size=mat.size(), i, j;
    this->ui->tableWidget->setColumnCount(size);
    this->ui->tableWidget->setRowCount(size);

    if (para=="interpola"){
        this->ui->verticalLayout->parentWidget()->setWindowTitle("Tabla de diferencias divididas.");
        this->ui->label->setText("Se lee hacia abajo");
        for(i=0;i<size;i++){
            for(j=0;j<size-i;j++){
                ui->tableWidget->setItem(i,j,new QTableWidgetItem(QString::number(mat[j][i])));
                }
            }
        }
    else if (para=="integra"){
        this->ui->verticalLayout->parentWidget()->setWindowTitle("Tabla de Integrales de Rombert.");
        this->ui->label->setText("Se lee hacia la derecha");

        for(i=0;i<size;i++){
            for(j=0;j<=i;j++){
                ui->tableWidget->setItem(i-j,j,new QTableWidgetItem(QString::number(mat[i][j])));
                }
            }
        }

}

FormInterIter::~FormInterIter()
{
    delete ui;
}

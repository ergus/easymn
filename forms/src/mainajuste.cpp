#include "mainajuste.h"
#include "ui_mainajuste.h"

#include "muParser.h"
#include "QFileDialog"
#include "QMessageBox"

#include "fstream"
#include "vector"

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"

#include "ajusfoo.h"
#include "fitter.h"
#include "polinomio.h"
#include "string.h"
#include "QTableWidget"
#include "maininterpol.h"


using namespace std;

MainAjuste::MainAjuste(QTableWidget *table, QWidget *parent,QMdiArea *parent2) :
    QMainWindow(parent),myparent(parent2),
    ui(new Ui::MainAjuste), parser(NULL)
{
    ui->setupUi(this);
    nommet=ui->rbPol->text();
    metodo=ui->rbPol->objectName();
    //from=ui->rbMan->objectName();
   

    cambiagrad(1);
    this->ui->tableWidget_3->setHorizontalHeaderItem(0,new QTableWidgetItem("x"));
    this->ui->tableWidget_3->setHorizontalHeaderItem(1,new QTableWidgetItem("y"));

    connect(this->ui->rbPol,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
    connect(this->ui->rbPred,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
    connect(this->ui->rbLin,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));

    connect(this->ui->enTerm,SIGNAL(valueChanged(int)),this,SLOT(cambiaterm(int)));
    connect(this->ui->egrado,SIGNAL(valueChanged(int)),this,SLOT(cambiagrad(int)));

    connect(this->ui->rbMan,SIGNAL(toggled(bool)),this,SLOT(cambiafrom(bool)));
    connect(this->ui->rbFun,SIGNAL(toggled(bool)),this,SLOT(cambiafrom(bool)));
    connect(this->ui->rbFich,SIGNAL(toggled(bool)),this,SLOT(cambiafrom(bool)));

    connect(this->ui->enpuntos,SIGNAL(valueChanged(int)),this,SLOT(setndat(int)));
    connect(this->ui->butLlen,SIGNAL(clicked()),this,SLOT(llenar()));
    connect(this->ui->butCalc,SIGNAL(clicked()),this,SLOT(calcular()));

    connect(this->ui->actionPng ,SIGNAL(triggered()),this,SLOT(export1())) ;
    connect(this->ui->actionJpg ,SIGNAL(triggered()),this,SLOT(export2())) ;
    connect(this->ui->actionEps ,SIGNAL(triggered()),this,SLOT(export3())) ;

    connect(this->ui->actionGuardar_Resumen,SIGNAL(triggered()),this,SLOT(resumen()));

    connect(this->ui->butIrAInterpol, SIGNAL(clicked()),this,SLOT(IrAInterpol()));
    
    connect(this->ui->actionAbrir,SIGNAL(triggered()),this,SLOT(Abrir()));
    
    
    this->ui->rbMan->toggle();

    models=llenamodels();

    connect(this->ui->cbfun,SIGNAL(currentIndexChanged(int)),this,SLOT(cambiamodel(int)));
    connect(this->ui->eposx,SIGNAL(textChanged(QString)),this,SLOT(cambiaposx(QString)));
    connect(this->ui->actionGuardar_Datos ,SIGNAL(triggered()),this,SLOT(exportdatos()));

    //Para el grafico--------------------
    fx=new mglData(50);
    fy=new mglData(50);

    QMGL = new QMathGL(this->ui->scrollArea_3);
    foo=new ajusfoo(&dx,&dy,&pfx,&pfy);

    QMGL->setDraw(foo);// for instance of class Foo:public mglDraw
    this->ui->scrollArea_3->setWidget(QMGL);
	
    this->QMGL->addAction(this->ui->actionJpg);
    this->QMGL->addAction(this->ui->actionPng);
    this->QMGL->addAction(this->ui->actionEps);
    this->QMGL->setContextMenuPolicy(Qt::ActionsContextMenu);
    
    this->QMGL->hide();
    
    this->ui->tableWidget_3->addAction(this->ui->actionGuardar_Datos);
    //-----------------------------
    connect(this->QMGL,SIGNAL(mouseClick(mreal,mreal,mreal)),this,SLOT(cambiagraf(mreal,mreal,mreal)));
    if(table!=NULL){
	int rows=table->rowCount();
	this->ui->enpuntos->setValue(rows);
	this->ui->tableWidget_3->setRowCount(rows);
	for (int i=0;i<rows;i++){
		for(int j=0;j<2;j++){
			if(table->item(i,j)!=0){
				this->ui->tableWidget_3->setItem(i,j,new QTableWidgetItem(table->item(i,j)->text()));
				}
			
			}
		}
	}

}

MainAjuste::~MainAjuste()
{
    delete ui;
    //delete foo;
    delete QMGL;
    delete fx;
    delete fy;
    delete parser;
}


void MainAjuste::cambiamet(bool){
    this->ui->eposx->setEnabled(false);
    QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
    if (radio) {
        metodo=radio->objectName();
        nommet=radio->text();

        this->ui->eFun->clear();
        this->ui->twmod->clear();
        this->ui->twfunout->clear();
        if(metodo=="rbPol"){
            this->ui->egrado->setEnabled(true);
            this->ui->cbfun->setEnabled(false);
            this->ui->twmod->setEnabled(false);
            this->ui->enTerm->setEnabled(false);
            cambiagrad(this->ui->egrado->value());
            }
        else if(metodo=="rbPred"){
            this->ui->egrado->setEnabled(false);
            this->ui->cbfun->setEnabled(true);
            this->ui->twmod->setEnabled(false);
            this->ui->enTerm->setEnabled(false);
            }
        else if(metodo=="rbLin"){
            this->ui->egrado->setEnabled(false);
            this->ui->cbfun->setEnabled(false);
            this->ui->twmod->setEnabled(true);
            this->ui->enTerm->setEnabled(true);
            cambiaterm(1);
            }
        this->ui->cbfun->setCurrentIndex(0);
        this->ui->efunres->clear();
        //QMGL->hide();

    }
}

void MainAjuste::cambiaterm(int nter){
    this->ui->eposx->setEnabled(false);
    this->ui->twmod->clear();
    this->ui->twmod->showColumn(1);
    this->ui->twmod->setColumnCount(2*nter);

    this->ui->twfunout->clear();
    this->ui->twfunout->setColumnCount(2*nter);

    this->ui->twmod->setItem(0,0,new QTableWidgetItem("C0*"));
    this->ui->twmod->setColumnWidth(0,40);

    this->ui->twfunout->setItem(0,0,new QTableWidgetItem("C0="));
    this->ui->twfunout->setColumnWidth(0,40);

    this->ui->twmod->item(0,0)->setFlags(!Qt::ItemIsEditable);

    this->ui->twmod->setColumnWidth(1,50);


    for(unsigned int i=1;i<nter;i++){
        this->ui->twmod->setItem(0,2*i,new QTableWidgetItem("+C"+QString::number(i)+"*"));
        this->ui->twmod->item(0,2*i)->setFlags(!Qt::ItemIsEditable);
        this->ui->twmod->setColumnWidth(2*i,47);
        this->ui->twmod->setColumnWidth(2*i+1,60);

        this->ui->twfunout->setItem(0,2*i,new QTableWidgetItem("C"+QString::number(i)+"="));
        this->ui->twfunout->setColumnWidth(2*i,47);
        }

    }

void MainAjuste::cambiagrad(int grad){
    this->ui->eposx->setEnabled(false);
    cambiaterm(grad+1);
    this->ui->enTerm->setValue(grad+1);
    this->ui->twmod->hideColumn(1);
    this->ui->twmod->item(0,0)->setText("C0");
    this->ui->twmod->setItem(0,3,new QTableWidgetItem("x"));
    this->ui->twmod->setColumnWidth(3,20);
    for(unsigned int i=2;i<grad+1;i++){
        this->ui->twmod->setItem(0,2*i+1,new QTableWidgetItem("x^"+QString::number(i)));
        }
    }

void MainAjuste::cambiafrom(bool){
    this->ui->eposx->setEnabled(false);
    QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
    if (radio) {
        from=radio->objectName();
        if(from=="rbMan"){
            this->ui->eFun->setDisabled(true);
            this->ui->emin_3->setDisabled(true);
            this->ui->emax_3->setDisabled(true);
            this->ui->enpuntos->setDisabled(false);
            this->ui->butCalc->setDisabled(false);
            this->ui->butLlen->setDisabled(true);
            this->ui->tableWidget_3->setEnabled(true);
            }
        else if(from=="rbFich"){
            this->ui->eFun->setDisabled(true);
            this->ui->emin_3->setDisabled(true);
            this->ui->emax_3->setDisabled(true);
            this->ui->enpuntos->setDisabled(true);
            this->ui->butCalc->setDisabled(true);
            this->ui->butLlen->setDisabled(true);
            this->ui->tableWidget_3->setEnabled(false);
	    if(this->ui->rbFich->isChecked()){
		QString nomfile= QFileDialog::getOpenFileName(this,"Abrir fichero con datos");
		if(!nomfile.isEmpty()){
			if(importar(nomfile)){
				ui->butCalc->setEnabled(true);
				}
			}
		else{
			this->ui->rbMan->setChecked(true);
			}
		}
            }
        else if("rbFun"){
            this->ui->eFun->setDisabled(false);
            this->ui->emin_3->setDisabled(false);
            this->ui->emax_3->setDisabled(false);
            this->ui->enpuntos->setDisabled(false);
            this->ui->butCalc->setDisabled(true);
            this->ui->butLlen->setDisabled(false);
            this->ui->tableWidget_3->setEnabled(false);
            }

    }
}

void MainAjuste::setndat(int a){
ui->tableWidget_3->setRowCount(a);
}

void MainAjuste::llenar(){

    if(from=="rbFun"){
        double min=ui->emin_3->text().toDouble(), max=ui->emax_3->text().toDouble();
        if(min!=max){
            if(min>max){
                double tem=max;
                max=min;
                min=tem;
                }
            int nodos=ui->enpuntos->value(), cont=0;
            double paso=(max-min)/(nodos-1), x=min, y;
            mu::Parser parser;
            parser.SetExpr((ui->eFun->text().toStdString().c_str()));
            parser.DefineVar("x", &x);
            for(cont=0;cont<nodos;cont++){
             try{
                y=parser.Eval();
                }
              catch(mu::Parser::exception_type &e){
                  cout << e.GetMsg() << endl;
                  return;
                }
                ui->tableWidget_3->setItem(cont,0,new QTableWidgetItem(QString::number(x)));
                ui->tableWidget_3->setItem(cont,1,new QTableWidgetItem(QString::number(y)));
                x+=paso;
                }
            ui->butCalc->setEnabled(true);
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("Error de intervalo");
            msgBox.exec();
            return;
            }
        QMGL->hide();
        }
    QMGL->update();
    }

bool MainAjuste::importar(QString nfile){
    ifstream file(nfile.toStdString().c_str());
    if(!file.good()){
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                return false;
                }
            else{
                int cont=0;
                ui->tableWidget_3->setRowCount(cont);
                vector<double> vx(0), vy(0);
                try{
                    double x, y;
                    file >> x >> y;
                    while(!file.eof()) {

                       ui->tableWidget_3->insertRow(cont);
                       ui->tableWidget_3->setItem(cont,0,new QTableWidgetItem(QString::number(x)));
                       ui->tableWidget_3->setItem(cont,1,new QTableWidgetItem(QString::number(y)));
                       vx.push_back(x);
                       vy.push_back(y);
                       cont++;
                       file >> x >> y;
                       }
                    ui->enpuntos->setValue(cont);

                    dx.Set(vx);
                    dy.Set(vy);
                    QMGL->setSize(ui->scrollArea_3->width(),ui->scrollArea_3->height());
                    QMGL->update();
                    QMGL->updateGeometry();
                    QMGL->show();
                   }
                catch(...){
                    QMessageBox msgBox;
                    msgBox.setText("Error de importacion");
                    msgBox.exec();
                    file.close();
                    return false;
                    }
                file.close();
                return true;
            }
}


vector<vector<string> > MainAjuste::llenamodels(){
    ifstream file("models.txt");
    vector<vector<string> > a;
    if(!file.good()){
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo de modelos");
                msgBox.exec();
                return a;
                }
            else{
                int nter,i,con=0;
                try{
                    string tem;
                    QString cad="";
                    file >> nter;
                    vector<string> vtem(0);
                    while(!file.eof()) {
                        for(i=0;i<nter;i++){
                            file>>tem;
                            vtem.push_back(tem);
                            cad=cad+"+C"+QString::number(i)+"*"+QString::fromStdString(tem);
                            }
                        cad.remove(0,1);
                        this->ui->cbfun->addItem(cad);
                        a.push_back(vtem);
                        cad.clear();
                        vtem.clear();
                        con++;
                        file>>nter;
                       }
                   }
                catch(...){
                    QMessageBox msgBox;
                    msgBox.setText("Error de importacion");
                    msgBox.exec();
                    file.close();
                    return a;
                    }
                file.close();
                return a;
            }
}

void MainAjuste::cambiamodel(int ind){
    this->ui->eposx->setEnabled(false);
    if(ind>0){
        int size=(models[ind-1]).size();
        cambiaterm(size);
        for(unsigned int i=0;i<size;i++){
            this->ui->twmod->setItem(0,2*i+1,new QTableWidgetItem(QString::fromStdString(models[ind-1][i])));
            }
        }
    else
        cambiaterm(1);

}

void MainAjuste::calcular(){
    int nodos=this->ui->enpuntos->value(), ntersol;
    unsigned int i;
    vector<double> x(nodos), y(nodos),sol;
    if((this->ui->tableWidget_3->item(0,0)!=0) && (this->ui->tableWidget_3->item(0,1)!=0)){
        x[0]=this->ui->tableWidget_3->item(0,0)->text().toDouble();
        y[0]=this->ui->tableWidget_3->item(0,1)->text().toDouble();
        }
    else{
        QMessageBox msgBox;
        msgBox.setText("Error en los Datos de pantalla, revíselos (parece que no hay datos)");
        msgBox.exec();
        return;
        }

    for(unsigned int i=1;i<nodos;i++){
        try{
            if((this->ui->tableWidget_3->item(i,0)!=0) && (this->ui->tableWidget_3->item(i,1)!=0)){
                x[i]=this->ui->tableWidget_3->item(i,0)->text().toDouble();
                y[i]=this->ui->tableWidget_3->item(i,1)->text().toDouble();
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("Error en los Datos de pantalla, faltan datos. Agrégelos o ajuste la tabla a la cantidad de datos reales de trabajo.");
                msgBox.exec();
                return;
                }
            }
        catch(...){
            QMessageBox msgBox;
            msgBox.setText("Error en los Datos de pantalla, revíselos");
            msgBox.exec();
            return;
            }
        }
    fitter fit(x,y);
        //Ac\E1 debo empezar construyendo el fitter
    if(metodo=="rbPol"){
        pol=fit.polinomialfit(this->ui->egrado->value());
        sol=pol.getpol();
        this->ui->efunres->setText(QString::fromStdString(pol.tostring()));
        ntersol=sol.size();
        }
    else{
        if(metodo=="rbPred"){
            int ind=this->ui->cbfun->currentIndex();
            if(ind>0)
                sol=fit.linealmodel(models[ind-1]);

            else{
                QMessageBox msgBox;
                msgBox.setText("Tiene que escoger una función de trabajo");
                msgBox.exec();
                return;
                }
            }
        else if(metodo=="rbLin"){
            int nter=this->ui->enTerm->value();
            vector<string> ent(0);
            for(i=0;i<nter;i++){
                if(this->ui->twmod->item(0,2*i+1)!=0){
                    ent.push_back(this->ui->twmod->item(0,2*i+1)->text().toStdString());
                    }
                else{
                    QMessageBox msgBox;
                    msgBox.setText("Error en loas funciones del modelo. Faltan Funciones por poner");
                    msgBox.exec();
                    }
                }
            sol=fit.linealmodel(ent);
            }
        ntersol=sol.size();
        QString cad=(QString::number(sol[0]))+"*"+(this->ui->twmod->item(0,1)->text());

        for(i=1;i<ntersol;i++){
            if(sol[i]>=0)
                cad+="+";
            cad=cad+(QString::number(sol[i]))+"*"+(this->ui->twmod->item(0,2*i+1)->text());
            }
        this->ui->efunres->setText(cad);
        if(parser!=NULL)
            delete parser;

        parser=new mu::Parser();
        parser->SetExpr(cad.toStdString().c_str());
        parser->DefineVar("x", &evx);
        }

    for(i=0;i<ntersol;i++){
        this->ui->twfunout->setItem(0,2*i+1,new QTableWidgetItem(QString::number(sol[i])));
        }
    dx.Set(x);
    dy.Set(y);

    double min=dx.Minimal(), max=dx.Maximal(), h=(max-min)/49;
    for(i=0;i<50;i++){
        fx->a[i]=min+i*h;
        fy->a[i]=this->evaluador(min+i*h);
        }
    pfx=fx;
    pfy=fy;
        QMGL->setSize(ui->scrollArea_3->width(),ui->scrollArea_3->height());
        QMGL->update();
        QMGL->updateGeometry();
        QMGL->show();
        this->ui->eposx->clear();
    this->ui->eposx->setEnabled(true);
    }

void MainAjuste::resizeEvent(QResizeEvent *){
    QMGL->setSize(ui->scrollArea_3->width(),ui->scrollArea_3->height());
    QMGL->update();
    QMGL->updateGeometry();
    }
void MainAjuste::cambiaposx(QString a){
	evx=a.toDouble();
	foo->setx(evx);
	ui->lposy->setText(QString::number(evaluador(evx)));
	QMGL->update();    
	}

void MainAjuste::cambiagraf(mreal a, mreal, mreal){
	if(this->ui->eposx->isEnabled()){
		this->ui->eposx->setText(QString::number(a));
		}
	}


void MainAjuste::export1(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
    if (!fileName.isEmpty()){
            QMGL->exportPNG(fileName);
            }
    }

void MainAjuste::export2(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
    if (!fileName.isEmpty()){
            QMGL->exportJPG(fileName);
            }
    }

void MainAjuste::export3(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
    if (!fileName.isEmpty()){
            QMGL->exportEPS(fileName);
            }
    }
    
void MainAjuste::IrAInterpol(){
	MainInterpol *Interpol=new MainInterpol(this->ui->tableWidget_3,parentWidget(),myparent);
	Interpol->setAttribute(Qt::WA_DeleteOnClose);
	myparent->addSubWindow(Interpol);
	Interpol->showMaximized();
	this->close();
	}

void MainAjuste::resumen(){
    QString nomfile= QFileDialog::getSaveFileName(this,"Abrir fichero con datos");
    if(!nomfile.isNull()){
        ofstream file(nomfile.toStdString().c_str(),ios::app);
        if(file.is_open()){
            if(file.good()){
                file<<"Ajuste de curvas con el modelo: f(x)=";
                for(int i=0;i<this->ui->twmod->columnCount();i++){
                    if(this->ui->twmod->item(0,i)!=0){

                        file<<this->ui->twmod->item(0,i)->text().toStdString();
                        }
                    else if(metodo=="rbPol" and i==1)
                        continue;
                    else{
                        QMessageBox msgBox;
                        msgBox.setText("Error en el modelo");
                        msgBox.exec();
                        return;
                        }
                    }file<<endl;
                if(from=="rbMan" || from=="rbFich"){
                    int a=ui->tableWidget_3->rowCount(),i;
                    file<<"Para "<<this->ui->enpuntos->text().toStdString()<<" datos:"<<endl;
                    file<<"x    \ty"<<endl;
                    for(i=0;i<a;i++){
                        file<<(this->ui->tableWidget_3->item(i,0)->text().toStdString())<<"\t"<<this->ui->tableWidget_3->item(i,1)->text().toStdString()<<endl;
                        }
                    }
                else if(from=="rbFun"){
                    file<<"Para "<<this->ui->enpuntos->text().toStdString()<<" puntos de la función ";
                    file<<this->ui->eFun->text().toStdString();
                    file<<" en el intervalo ["<< this->ui->emin_3->text().toStdString();
                    file<<";"<< this->ui->emax_3->text().toStdString() <<"]."<<endl;
                    }

                file<<"\nResultados:\n";
                int nter=this->ui->enTerm->value();
                for(int i=0;i<nter;i++){
                    file<<"C"<<i<<"=\t"<<this->ui->twfunout->item(0,2*i+1)->text().toStdString()<<endl;
                    }
                file<<"\nY la función: f(x)="<<this->ui->efunres->text().toStdString()<<endl;

                file<<"------------------------------------------------"<<endl;
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                }
            file.close();
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("No se pudo abrir el archivo");
            msgBox.exec();
            }
        }
    }

void MainAjuste::exportdatos(){
    int ndat=this->ui->tableWidget_3->rowCount();
    QString nomfile= QFileDialog::getSaveFileName(this,"Abrir fichero con datos");
    if(!nomfile.isNull()){
        ofstream file(nomfile.toStdString().c_str(),ios::app);
        if(file.is_open()){
            if(file.good()){
                for(int i=0;i<ndat;i++){
                    if((this->ui->tableWidget_3->item(i,0)!=0) && (this->ui->tableWidget_3->item(i,1)!=0)){
                        file<<(this->ui->tableWidget_3->item(i,0)->text().toStdString());
                        file<<"\t"<<(this->ui->tableWidget_3->item(i,1)->text().toStdString())<<endl;
                        }
                    else{
                        QMessageBox msgBox;
                        msgBox.setText("Error al exportar los datos, faltan datos en pantalla");
                        msgBox.exec();
                        file.close();
                        return;
                        }
                    }
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                }
            file.close();
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("No se pudo abrir el archivo");
            msgBox.exec();
            }
        }
    }

void MainAjuste::Abrir(){
	this->ui->rbFich->setChecked(true);
	}
	
double MainAjuste::evaluador(double a){
	if(metodo=="rbPol")
		return this->pol.eval(a);
        else{
                evx=a;
                return parser->Eval();
                }
        }
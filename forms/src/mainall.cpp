#include "mainall.h"
#include "ui_mainall.h"
#include "mainplot.h"
#include "mainraiz.h"
#include "mainsistema.h"
#include "maininterpol.h"
#include "mainajuste.h"
#include "mainintegra.h"
#include "mainedo.h"
#include "mainall.h"
#include "about.h"
#include "QDesktopServices"

MainALL::MainALL(QWidget *parent) :
    QMainWindow(parent), pl(NULL),
    ui(new Ui::MainALL)
{
    ui->setupUi(this);
    this->setCentralWidget(this->ui->mdiArea);
    connect(this->ui->actionAjuste_de_curvas,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionCalcular_Raices,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionEcuaciones_Diferenciales,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionGraficador,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionIntegracion,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionInterpolacion,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionSistemas_Lineales,SIGNAL(triggered()),this, SLOT(abrevent()));
    connect(this->ui->actionSobre_el_Programa,SIGNAL(triggered()),this, SLOT(abreabout()));
    connect(this->ui->actionAyuda,SIGNAL(triggered()),this,SLOT(abreayuda()));

}

MainALL::~MainALL()
{
    delete ui;
    delete pl;

}
void MainALL::addvent(QMainWindow* ve){
    this->ui->mdiArea->closeAllSubWindows();
    pl=ve;
    this->ui->mdiArea->addSubWindow(pl);
    pl->setAttribute(Qt::WA_DeleteOnClose);
    pl->showMaximized();
    }

void MainALL::abrevent(){
QAction *action = qobject_cast< QAction* >(QObject::sender());
if(action){
    this->ui->mdiArea->closeAllSubWindows();
    QString vent=action->objectName();
    if(vent=="actionAjuste_de_curvas"){
        pl=new MainAjuste(NULL,this->ui->mdiArea,this->ui->mdiArea);
        }
    else if(vent=="actionGraficador"){
        pl=new MainPlot(this->ui->mdiArea,this);
        }
    else if(vent=="actionCalcular_Raices"){
        pl=new MainRaiz(this->ui->mdiArea);
        }
    else if(vent=="actionEcuaciones_Diferenciales"){
        pl=new MainEDO(this->ui->mdiArea);
        }
    else if(vent=="actionIntegracion"){
        pl=new MainIntegra(this->ui->mdiArea);
        }
    else if(vent=="actionInterpolacion"){
        pl=new MainInterpol(NULL,this->ui->mdiArea,this->ui->mdiArea);
        }
    else if(vent=="actionSistemas_Lineales"){
        pl=new MainSistema(this->ui->mdiArea);
        }
    this->ui->mdiArea->addSubWindow(pl);
    pl->setAttribute(Qt::WA_DeleteOnClose);
    pl->showMaximized();
    }
}

void MainALL::abreabout(){
    About *ab=new About();
    ab->show();
    }

void MainALL::abreayuda(){
	QDir helpdir("Documentation");
	if(helpdir.exists()){
		QDesktopServices::openUrl(QUrl("Documentation/html/index.html", QUrl::TolerantMode));
		}
	else {
		QDesktopServices::openUrl(QUrl("../share/EasyMN/doc/html/index.html", QUrl::TolerantMode));
		}
	};

#include "formplotiter.h"
#include "ui_formplotiter.h"
#include "mgl2/qt.h"
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "itfoo.h"
#include "QFileDialog"
#include "iostream"

FormPlotIter::FormPlotIter(vector<vector<double> > iter, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormPlotIter)
{
    ui->setupUi(this);

    itdata.Set((iter[0]));
    iterr.Set((iter[1]));

    //this->graficar();

    connect(ui->buteps,SIGNAL(clicked()),this,SLOT(export3()));
    connect(ui->butjpg,SIGNAL(clicked()),this,SLOT(export2()));
    connect(ui->butpng,SIGNAL(clicked()),this,SLOT(export1()));

}

FormPlotIter::~FormPlotIter()
{
    delete itQMGL;
    delete ui;
}




void FormPlotIter::graficar(){
    itfoo foo(itdata,iterr);

    itQMGL=new QMathGL(this->ui->scrollArea);

    itQMGL->setDraw(&foo);// for instance of class Foo:public mglDraw

    itQMGL->setSize(this->ui->scrollArea->width(),ui->scrollArea->height());
    this->ui->scrollArea->setWidget(itQMGL);
}

void FormPlotIter::show(){
    this->QWidget::show();
    this->graficar();

    }
//-----Exportar------------------
void FormPlotIter::export1(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
    if (!fileName.isEmpty()){
            itQMGL->exportPNG(fileName);
            }
    }

void FormPlotIter::export2(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
    if (!fileName.isEmpty()){
            itQMGL->exportJPG(fileName);
            }
    }

void FormPlotIter::export3(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
    if (!fileName.isEmpty()){
            itQMGL->exportEPS(fileName);
            }
    }

#include "mainintegra.h"
#include "ui_mainintegra.h"
#include "integrador.h"

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"

#include "muParser.h"
#include "integrador.h"
#include "integfoo.h"

#include "QFileDialog"
#include "QMessageBox"

#include "forminteriter.h"

using namespace std;
MainIntegra::MainIntegra(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainIntegra),metodo("rbtrap"),parser(NULL)
{
	ui->setupUi(this);

	connect(this->ui->rbgauss,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
	connect(this->ui->rbrom,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
	connect(this->ui->rbsim,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));
	connect(this->ui->rbtrap,SIGNAL(toggled(bool)),this,SLOT(cambiamet(bool)));

	connect(this->ui->usesimpsons,SIGNAL(toggled(bool)),this,SLOT(cambiarombert(bool)));
	connect(this->ui->usetrapecio,SIGNAL(toggled(bool)),this,SLOT(cambiarombert(bool)));

	connect(this->ui->vertabla_but,SIGNAL(clicked()),this,SLOT(vertabla()));

	connect(this->ui->eh,SIGNAL(textChanged(QString)),this,SLOT(cambiah(QString)));
	connect(this->ui->emax,SIGNAL(textChanged(QString)),this,SLOT(cambiah(QString)));
	connect(this->ui->emin,SIGNAL(textChanged(QString)),this,SLOT(cambiah(QString)));
	connect(this->ui->enpasos,SIGNAL(textChanged(QString)),this,SLOT(cambiapasos(QString)));
	connect(this->ui->butcalc,SIGNAL(clicked()),this,SLOT(Calcular()));

	connect(this->ui->actionPng ,SIGNAL(triggered()),this,SLOT(export1())) ;
	connect(this->ui->actionJpg ,SIGNAL(triggered()),this,SLOT(export2())) ;
	connect(this->ui->actionEps ,SIGNAL(triggered()),this,SLOT(export3())) ;

	connect(this->ui->actionGuardar_Resumen ,SIGNAL(triggered()),this,SLOT(resumen()));

	this->ui->eh->setText(QString::number(0.01));
	this->ui->eerror->setText(QString::number(0.01));

	fx=new mglData(50);
	fy=new mglData(50);

	QMGL = new QMathGL(this->ui->scrollArea);
	foo=new integfoo(fx,fy);

	QMGL->setDraw(foo);// for instance of class Foo:public mglDraw
	this->ui->scrollArea->setWidget(QMGL);
	QMGL->hide();

	this->QMGL->addAction(this->ui->actionJpg);
	this->QMGL->addAction(this->ui->actionPng);
	this->QMGL->addAction(this->ui->actionEps);
	this->QMGL->setContextMenuPolicy(Qt::ActionsContextMenu);
	
	this->ui->rbtrap->toggle();

	}

MainIntegra::~MainIntegra()
{
    delete ui;
    delete QMGL;
    //delete foo;
    delete fx;
    delete fy;
}

void MainIntegra::cambiamet(bool){
    QRadioButton *radio = qobject_cast< QRadioButton* >(QObject::sender());
    if (radio) {
        sal.clear();
        this->ui->vertabla_but->hide();
        this->metodo=radio->objectName();
        this->nomet=radio->text();
        if((metodo=="rbtrap") || (metodo=="rbsim")){
            this->ui->eh->setEnabled(true);
            this->ui->enpasos->setEnabled(true);
            this->ui->eorden->setEnabled(false);
            this->ui->eerror->setEnabled(false);
            this->ui->widget_rombert->setEnabled(false);
            }
        else if(metodo=="rbrom"){
            this->ui->eh->setEnabled(true);
            this->ui->enpasos->setEnabled(true);
            this->ui->eorden->setEnabled(false);
            this->ui->eerror->setEnabled(true);
            this->ui->widget_rombert->setEnabled(true);
            }
        else if(metodo=="rbgauss"){
            this->ui->eh->setEnabled(false);
            this->ui->enpasos->setEnabled(false);
            this->ui->eorden->setEnabled(true);
            this->ui->eerror->setEnabled(false);
            this->ui->widget_rombert->setEnabled(false);
            }
        }
    }

void MainIntegra::cambiah(QString val){
    if((!this->ui->emax->text().isEmpty()) && (!this->ui->emin->text().isEmpty()) && (!this->ui->eh->text().isEmpty())){
        double max=this->ui->emax->text().toDouble(), min=this->ui->emin->text().toDouble(), dval=this->ui->eh->text().toDouble();
        this->ui->enpasos->setText(QString::number(int(max-min)/dval));
        }
    }

void MainIntegra::cambiapasos(QString val){
    double max=this->ui->emax->text().toDouble(), min=this->ui->emin->text().toDouble(), dval=val.toDouble();;
    this->ui->eh->setText(QString::number((max-min)/dval));
    }

void MainIntegra::Calcular(){
    if(parser!=NULL)
        delete parser;
    parser=new mu::Parser();
    parser->SetExpr(this->ui->efun->text().toStdString().c_str());
    parser->DefineVar("x", &x);
    min=this->ui->emin->text().toDouble();
    max=this->ui->emax->text().toDouble();
    double h=this->ui->eh->text().toDouble();
    replot();
    QMGL->show();
    //Principal
    res resul(0.,0.);

    integrador integ(this->ui->efun->text().toStdString(),min, max, h);
    if(metodo=="rbtrap"){
        resul=integ.trapecios();
        }
    else if(metodo=="rbsim"){
        resul=integ.simpson();
        }
    else if(metodo=="rbgauss"){
        resul=integ.gaussfull(this->ui->eorden->value());
        }
    else if(metodo=="rbrom"){
        sal.clear();
        if(this->ui->usetrapecio->isChecked()){
            resul=integ.rombert(this->ui->eerror->text().toDouble(),sal,string("trapecios"));
            }
        if(this->ui->usesimpsons->isChecked()){
            resul=integ.rombert(this->ui->eerror->text().toDouble(),sal,string("simpsons"));
            }
        this->ui->vertabla_but->show();
        }
    this->ui->lres->setText(QString::number(resul.val)+"+/-"+QString::number(resul.err));
    }

void MainIntegra::replot(){
    double lh=(max-min)/49;
    x=min;
    for(int i=0;i<50;i++){
        fx->a[i]=x;
        try{
            fy->a[i]=parser->Eval();
            }
        catch(mu::Parser::exception_type &e){
            cout << e.GetMsg() << endl;

            return;
          }
        x+=lh;
        }
    QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
    QMGL->update();
    QMGL->updateGeometry();
    }

void MainIntegra::export1(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato png");
    if (!fileName.isEmpty()){
            this->QMGL->exportPNG(fileName);
            }
    }

void MainIntegra::export2(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato jpg");
    if (!fileName.isEmpty()){
            this->QMGL->exportJPG(fileName);
            }
    }

void MainIntegra::export3(){
    QString fileName = QFileDialog::getSaveFileName(this,"Guardar en formato eps");
    if (!fileName.isEmpty()){
            this->QMGL->exportEPS(fileName);
            }
    }

void MainIntegra::resumen(){
    QString nomfile= QFileDialog::getSaveFileName(this,"Abrir fichero con datos");
    if(!nomfile.isNull()){
        ofstream file(nomfile.toStdString().c_str(),ios::app);
        if(file.is_open()){
            if(file.good()){
                file<<"Integración Numérica con el método de "<<nomet.toStdString()<<" ";
                if(metodo=="rbgauss"){
                    file<<"orden "<<this->ui->eorden->text().toStdString()<<". "<<endl;
                    }
                else{
                    if(metodo=="rbrom"){
                        if(this->ui->usetrapecio->isChecked() and sal.size()>0){
                            file<<"(+Trapecios) ";
                            }
                        else if(this->ui->usesimpsons->isChecked() and sal.size()>0){
                            file<<"(+Simpsons) ";
                            }
                        file<<"con cota de error e="<<this->ui->eerror->text().toStdString()<<".";
                        }
                    file<<"\nPaso: "<<this->ui->eh->text().toStdString()<<". ";
                    }
                file<<"En el intervalo: ["<<this->ui->emin->text().toStdString()<<";"<<this->ui->emax->text().toStdString()<<"]"<<endl;
                file<<"f(x)="<<this->ui->efun->text().toStdString()<<endl;
                file<<"Resultado: "<<this->ui->lres->text().toStdString()<<endl;
                if(metodo=="rbrom" and sal.size()>0){
                    file<<"\n Tabla de diferencia divididas"<<endl;
                    int size=sal.size();
                    for(int i=0;i<size;i++){
                        for(int j=0;j<=i;j++){
                            file<<sal[i][j]<<" ";
                            }
                        file<<"\n";
                        }

                    }
                file<<"------------------------------------------------"<<endl;
                }
            else{
                QMessageBox msgBox;
                msgBox.setText("No se pudo abrir el archivo");
                msgBox.exec();
                }
            file.close();
            }
        else{
            QMessageBox msgBox;
            msgBox.setText("No se pudo abrir el archivo");
            msgBox.exec();
            }
        }
    }

void MainIntegra::resizeEvent(QResizeEvent * event){
    QMGL->setSize(ui->scrollArea->width(),ui->scrollArea->height());
    QMGL->updateGeometry();
}

void MainIntegra::vertabla(){
    FormInterIter *vddif=new FormInterIter(sal,string("integra"));
    vddif->show();
    }

void MainIntegra::cambiarombert(bool){
    this->ui->vertabla_but->hide();
    sal.clear();
    }

#ifndef MAININTERPOL_H
#define MAININTERPOL_H

#include <QMainWindow>
#include "polinomio.h"
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"
#include "ajusfoo.h"
#include <QTableWidgetItem>
#include "mgl2/qmathgl.h"
#include "QMdiArea"

namespace Ui {
    class MainInterpol;
}

class MainInterpol : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainInterpol(QTableWidget *table, QWidget *parent, QMdiArea *parent2);
    ~MainInterpol();

private:
    Ui::MainInterpol *ui;
    int nodos;
    QString from, metodo, nommet;
    bool importar(QString nfile);
    polinomio polin;

    ajusfoo *foo;
    mglData dx, dy,*fx, *fy, pfx, pfy;
    double evx;

    vector<vector<double> > difdiv;
    QMathGL *QMGL;
    void graficar();
    void resizeEvent(QResizeEvent * event);
    QMdiArea *myparent;

private slots:
    void setrow(int);
    void cambiafrom(bool);
    void cambiamet(bool);
    void llenar();
    void calcular();
    void resumen();
    void export1();
    void export2();
    void export3();
    void verddif();
    void cambiaposx(QString);
    void tohide(QTableWidgetItem *);
    void cambiagraf(mreal a, mreal, mreal);
    void exportdatos();
    void IrAAjuste();
    void Abrir();

};



#endif // MAININTERPOL_H

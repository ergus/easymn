#ifndef MAINSISTEMA_H
#define MAINSISTEMA_H

#include <QMainWindow>
#include <vector>

namespace Ui {
    class MainSistema;
}
using namespace std;
class MainSistema : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainSistema(QWidget *parent = 0);
    ~MainSistema();

private:
    QString metodo;
    Ui::MainSistema *ui;
    int rango;
    bool importar(QString);
    vector<vector<double> > matrizs, iteracioness;
    vector<double> vectors, vecsalida;
    bool captapantalla();

private slots:
    void redimen(QString);
    void cambiametodo(bool);
    void cambiaarchi(bool);
    void cambiaarchi();
    void cambiaman(bool);
    void calcular();

    void exportar();
    void guardamat();
    void veriter();
};

#endif // MAINSISTEMA_H

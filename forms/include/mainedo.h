#ifndef MAINEDO_H
#define MAINEDO_H

#include <QMainWindow>
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"
#include "mgl2/qmathgl.h"
#include "ajusfoo.h"
#include "muParser.h"
#include "QEvent"

namespace Ui {
    class MainEDO;
}

class MainEDO : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainEDO(QWidget *parent = 0);
    ~MainEDO();
    void resizeEvent(QResizeEvent * event);

private:
    Ui::MainEDO *ui;
    QString metodo, nomet;
    QMathGL *QMGL;
    ajusfoo *foo;
    mglData *fx, *fy;
    mu::Parser *parser;

public slots:
    void cambiamet();
    void cambiaorden(int);
    void cambiah(QString);
    void cambiacell(int, int){cambiah(metodo);}
    void cambiapaso(QString);
    void calcular();
    void export1();
    void export2();
    void export3();
    void resumen();

};

#endif // MAINEDO_H

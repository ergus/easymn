#ifndef MAINRAIZ_H
#define MAINRAIZ_H

#include <QMainWindow>
#include "mgl2/qt.h"
#include <mgl2/qmathgl.h>
#include "muParser.h"
#include "QtGlobal"
#include "qstring.h"
#include "vector"
#include "myfoo.h"

namespace Ui {
    class MainRaiz;
}

class MainRaiz : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainRaiz(QWidget *parent = 0);
    explicit MainRaiz(QString fun, double min, double max, QWidget *parent = 0 );
    ~MainRaiz();

private:
    QString metodo, nombremetodo;
    Ui::MainRaiz *ui;
    QMathGL *rQMGL;
    double rmin, rmax, x, paso;
    mu::Parser* parser;
    void inicializa();
    void resizeEvent(QResizeEvent * event);
    std::vector<std::vector<double> > iter;
    mglData *datx, *daty;
    myfoo *foo;
    string cadfun;


private slots:
    void graficar();
    void rbcheck(bool);
    void calcular();


    void export1();
    void export2();
    void export3();

    void guardaraiz();

    void grafit();

};

#endif // MAINRAIZ_H

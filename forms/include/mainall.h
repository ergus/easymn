#ifndef MAINALL_H
#define MAINALL_H

#include <QMainWindow>
#include "QDesktopServices"
#include "QUrl"
#include "QKeyEvent"
#include "QtTest/QTest"
#include "QDir"

namespace Ui {
class MainALL;
}

class MainALL : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainALL(QWidget *parent = 0);
    ~MainALL();
    void addvent(QMainWindow* ve);
    void keyPressEvent(QKeyEvent *event){
            if (event->key() == Qt::Key_F1) {
                abreayuda();
                }
            }
    
private:
    Ui::MainALL *ui;
    QMainWindow *pl;

private slots:
    void abrevent();
    void abreabout();
    void abreayuda();
};
#endif // MAINALL_H

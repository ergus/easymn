#ifndef MAININTEGRA_H
#define MAININTEGRA_H

#include <QMainWindow>

#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"
#include "mgl2/qmathgl.h"
#include "integfoo.h"
#include "muParser.h"
#include "vector"

using namespace std;
namespace Ui {
    class MainIntegra;
}

class MainIntegra : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainIntegra(QWidget *parent = 0);
    ~MainIntegra();
    void resizeEvent(QResizeEvent * event);

private:
    double x, min, max;
    Ui::MainIntegra *ui;
    QString metodo, nomet;
    QMathGL *QMGL;
    integfoo *foo;
    mglData *fx, *fy;
    mu::Parser *parser;
    void replot();
    vector<vector<double> > sal;

private slots:
    void cambiamet(bool);
    void cambiah(QString);
    void cambiapasos(QString);
    void Calcular();
    void export1();
    void export2();
    void export3();
    void resumen();
    void vertabla();
    void cambiarombert(bool);

};

#endif // MAININTEGRA_H

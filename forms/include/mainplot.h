#ifndef MAINPLOT_H
#define MAINPLOT_H

#include <QMainWindow>
#include "mgl2/qt.h"
#include <mgl2/qmathgl.h>
#include "muParser.h"
#include "QtGlobal"
#include "qstring.h"
#include "myfoo.h"
#include "vector"
#include "mainall.h"

namespace Ui {
    class MainPlot;
}

class MainPlot : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainPlot(QWidget *parent = 0, MainALL* omall=NULL);
    ~MainPlot();
    MainALL* getmall(){return mall;}
    void resizeEvent(QResizeEvent * event);


private:
    Ui::MainPlot *ui;

    QMathGL *QMGL;
    mu::Parser* parser;
    double px;
    double pmin, pmax, paso;
    myfoo *foo;
    mglData *datx, *daty;
    MainALL* mall;


public slots:
    void setlaby(QString);
    void replot();
    void actpos(mreal, mreal, mreal);

    void export1();
    void export2();
    void export3();
    void iraraiz();
};

#endif // MAINPLOT_H

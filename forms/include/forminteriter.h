#ifndef FORMINTERITER_H
#define FORMINTERITER_H

#include <QWidget>
using namespace std;
namespace Ui {
    class FormInterIter;
}

class FormInterIter : public QWidget
{
    Q_OBJECT

public:
    explicit FormInterIter(vector<vector<double> > mat,string para,QWidget *parent = 0);
    ~FormInterIter();

private:
    Ui::FormInterIter *ui;
};

#endif // FORMINTERITER_H

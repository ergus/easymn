#ifndef MAINAJUSTE_H
#define MAINAJUSTE_H

#include <QMainWindow>
#include "vector"
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"
#include "mgl2/qmathgl.h"
#include "string"
#include "ajusfoo.h"
#include "polinomio.h"
#include "muParser.h"
#include "QTableWidget"
#include "QMdiArea"

using namespace std;

namespace Ui {
    class MainAjuste;
}


class MainAjuste : public QMainWindow
{
    Q_OBJECT

public:
	
    explicit MainAjuste(QTableWidget *table,QWidget *parent, QMdiArea *parent2);
    ~MainAjuste();

private:
    Ui::MainAjuste *ui;
    vector<vector<string> > models;
    QString metodo, nommet, from;
    bool importar(QString);
    QMathGL *QMGL;
    ajusfoo *foo;
    mglData dx, dy,*fx, *fy, pfx, pfy;
    double evx;
    polinomio pol;
    mu::Parser *parser;
    void resizeEvent(QResizeEvent *);
    QMdiArea *myparent;
    double evaluador(double a);

private slots:
    void cambiamet(bool);
    void cambiaterm(int nter);
    void cambiagrad(int grad);
    void cambiafrom(bool);
    void setndat(int);
    void llenar();
    vector<vector<string> > llenamodels();
    void cambiamodel(int);
    void calcular();
    void cambiaposx(QString);
    void cambiagraf(mreal a, mreal, mreal);
    void export1();
    void export2();
    void export3();
    void resumen();
    void exportdatos();
    void IrAInterpol();
    void Abrir();
};

#endif // MAINAJUSTE_H

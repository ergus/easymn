#ifndef FORMPLOTITER_H
#define FORMPLOTITER_H

#include <QWidget>
#include "mgl2/mgl.h"
#include "mgl2/wnd.h"
#include "mgl2/qt.h"
#include "mgl2/qmathgl.h"

using namespace std;
namespace Ui {
    class FormPlotIter;
}

class FormPlotIter : public QWidget
{
    Q_OBJECT

public:
    explicit FormPlotIter(vector<vector<double> > iter,QWidget *parent = 0);
    ~FormPlotIter();
    void show();
    void resizeEvent(){this->graficar();}

private:
    Ui::FormPlotIter *ui;
    mglData itdata, iterr;
    void graficar();
    QMathGL *itQMGL;

private slots:
    void export1();
    void export2();
    void export3();

};

#endif // FORMPLOTITER_H

#ifndef FORMSISITER_H
#define FORMSISITER_H

#include <QWidget>
#include <vector>

namespace Ui {
    class FormSisIter;
}

typedef std::vector<std::vector<double> > matri;

class FormSisIter : public QWidget
{
    Q_OBJECT

public:
    explicit FormSisIter(matri mat, QString, QWidget *parent = 0);
    ~FormSisIter();

private:
    Ui::FormSisIter *ui;
};

#endif // FORMSISITER_H
